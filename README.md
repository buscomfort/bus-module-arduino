# bus-module-arduino

Módulo embarcado para ficar no ônibus para o projeto busComfort

## Hardware utilizado

* STM32F103C8T6. Versões antigas tem suporte para  Arduino Uno - ATmega328P / Arduino Due - ATSAM3X8E
* Radioenge LoRaMESH
* Sensores:
    * GPS - NEO-7M
    * Sensor Humidade e Temperatura do Ar - DHT11
    * Acelerômetro e Giroscópio - MPU-6050
    * Sensor de Ruído (Microfone) - KY-038

## Requisitos

Para compilar esse sketch são necessários os seguintes softwares e bibliotecas adicionais:

* [Arduino](https://github.com/arduino/Arduino) >= 1.8.10
* [STM32 Cores](https://github.com/stm32duino/Arduino_Core_STM32) (Arduino core support for STM32 based boards) by stm32duino >= 1.7.0
* [SomeSerial](https://github.com/asukiaaa/SomeSerial) by Asuki Kono >= 1.1.1
* [SimpleDHT](https://github.com/winlinvip/SimpleDHT) by Winlin >= 1.0.12
  
Para compilar esse sketch para a placa de desenvolvimento principal, o STM32F103C8T6, pela IDE do Arduino é necessário adicionar o suporte às placas STM32 por meio da bibliotecas oficiais feitas, conforme listado acima.

### Versões antigas
As versões antigas do software também possuem suporte para o Arduino Uno e o Arduino Due.

Para compilar esse sketch para o Arduino Due pela IDE do Arduino é necessário também a adição da placa:

* [Arduino SAM Boards](https://github.com/arduino/ArduinoCore-sam) (32-bits ARM Cortex-M3) by Arduino >= 1.6.12

