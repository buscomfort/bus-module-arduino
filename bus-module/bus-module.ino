#include "src/BusModule.h"
#include "src/GpsModule.h"
#include "src/CommunicationModule/LoggerSerial.h"
#include "src/AccelerometerModule.h"
#include <SomeSerial.h>

// "sketch": "bus-module/bus-module.ino",
// "board": "STM32:stm32:GenF1",
// "configuration": "pnum=BLUEPILL_F103C8B,upload_method=swdMethod,xserial=generic,usb=none,xusb=HS,opt=osstd,rtlib=nano"

// The serial connection to the GPS module
BusModule *busModule = new BusModule();
LoggerSerial *logger = new LoggerSerial();

// busModule Sensors Configuration
/* Baudrate for software UART used for NEO-7M communication */
#define NEO7M_BAUDRATE 9600
#define NEO7M_TX_PIN 2 // Pinos somente para o Arduino UNO
#define NEO7M_RX_PIN 3 // A HardwareSerial Serial1 é utilizada no Arduino Due

// LoRaMESH Configuration
#define LORAMESH_COMM_BAUDRATE 9600
#define LORAMESH_TX_COMM_PIN 4 // Pinos somente para o Arduino UNO, Serial2 é utilizada no Arduino Due
#define LORAMESH_RX_COMM_PIN 5 // Pinos da interface de comando

#define LORAMESH_TRANS_BAUDRATE 9600
#define LORAMESH_TX_TRANS_PIN 6 // Pinos somente para o Arduino UNO, Serial3 é utilizada no Arduino Due
#define LORAMESH_RX_TRANS_PIN 7 // Pinos da interface transparente

#define LOGGER_BAUDRATE 115200
// #define LOGGER_BAUDRATE 9600

#define LED_SUCCESS PB15
#define LED_ERROR PB14

#define DEBUG_PIN_AVAILABLE false
#define DEBUG_PIN PA4

#define ROUTINE_TIME 30
#define ACCEL_HZ 500
#define NOISE_HZ 5000
#define ACCEL_PERIOD 2000 // in microseconds ainda não tá sendo passado como parâmetro
#define NOISE_PERIOD 2000 // in microseconds
#define SEND_RETRIES 3	// Execução da verificação do recebimento ACK e envio de um pacote.
#define TEMP_RETRIES 3
#define GPS_RETRIES 2

#if defined(STM32F1xx) || defined(STM32_SERIES_F1)
#define LED_BUILTIN PC13
// HardwareSerial Serial1(USART1);
HardwareSerial Serial2(USART2);
HardwareSerial Serial3(USART3);
SomeSerial hLogger(&Serial3);
SomeSerial *gpsSerial = new SomeSerial(&Serial);
// SomeSerial * commandSerial = new SomeSerial(&Serial3);
SomeSerial *transparentSerial = new SomeSerial(&Serial2);
#else
#if defined(ARDUINO_ARCH_SAM)
SomeSerial hLogger(&SerialUSB);
#else
SomeSerial hLogger(&Serial);
#endif
SomeSerial *gpsSerial = new SomeSerial(&Serial1);
// SomeSerial * commandSerial = new SomeSerial(&Serial2);
SomeSerial *transparentSerial = new SomeSerial(&Serial2);
#endif
/* Includes ---------------------- */
#include <stdint.h>
#include <stdbool.h>

/* Defines ----------------------- */
/* Payload buffer */
// uint8_t bufferPayload[MAX_PAYLOAD_SIZE] = {0};
// uint8_t busModulePayload[BUS_MODULE_PAYLOAD_SIZE] = {0};

void blink_led()
{
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);
	delay(500);
	digitalWrite(LED_BUILTIN, HIGH);
	delay(500);
	digitalWrite(LED_BUILTIN, LOW);
	delay(500);
	digitalWrite(LED_BUILTIN, HIGH);
}
/* Initialization routine */
void setup()
{
	blink_led();
	BusModuleType busModuleType;
	delay(500);
	if (DEBUG_PIN_AVAILABLE)
	{
		pinMode(DEBUG_PIN, INPUT);
		if (digitalRead(DEBUG_PIN) == HIGH)
		{
			logger->Setup(&hLogger, LOGGER_BAUDRATE);
		}
	}
	else
	{
		logger->Setup(&hLogger, LOGGER_BAUDRATE);
	}
	// logger->print("STARTING\n\n");
	busModule->SetupLogger(logger);

#if defined(STM32F1xx) || defined(STM32_SERIES_F1)
	busModuleType = BUSMOD_STM32F103C8_128K;
#elif defined(ARDUINO_ARCH_SAM)
	busModuleType = BUSMOD_ARDUINO_DUE;
#else
	busModuleType = BUSMOD_ARDUINO_UNO;
#endif
	// busModule->Setup(busModuleType);
	busModule->Setup(busModuleType, LED_SUCCESS, LED_ERROR, &Wire, gpsSerial, transparentSerial);
	busModule->SetupTimers(ROUTINE_TIME, SEND_RETRIES, TEMP_RETRIES, GPS_RETRIES, ACCEL_HZ, NOISE_HZ);
	// busModule->SetupTimerGpsOnly(ROUTINE_TIME, SEND_RETRIES, GPS_RETRIES);
	// busModule->SetupTimerAxcelOnly(ACCEL_HZ);
	// busModule->SetupTimerNoiseOnly(NOISE_HZ);
	// logger->print("STARTING\n\n");
}

/* Main loop */
void loop()
{
	// logger->print("NEW  ROUTINE\n\n");
	// busModule->ExecuteRoutine();

	// logger->print("END ROUTINE\n\n");
}
