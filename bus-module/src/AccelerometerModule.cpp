#include "AccelerometerModule.h"

LoggerSerial *AccelerometerModule::loggerSerial = NULL;

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "MPU6050/I2Cdev.h"

// #include "MPU6050/MPU6050_6Axis_MotionApps20.h"
#include "MPU6050/MPU6050_6Axis_MotionApps_V6_12.h"
//#include "MPU6050/MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

/* =========================================================================
   NOTE: In addition to connection 3.3v, GND, SDA, and SCL, this sketch
   depends on the MPU-6050's INT pin being connected to the Arduino's
   external interrupt #0 pin. On the Arduino Uno and Mega 2560, this is
   digital I/O pin 2.
 * ========================================================================= */

/* =========================================================================
   NOTE: Arduino v1.0.1 with the Leonardo board generates a compile error
   when using Serial.write(buf, len). The Teapot output uses this method.
   The solution requires a modification to the Arduino USBAPI.h file, which
   is fortunately simple, but annoying. This will be fixed in the next IDE
   release. For more info, see these links:
   http://arduino.cc/forum/index.php/topic,109987.0.html
   http://code.google.com/p/arduino/issues/detail?id=958
 * ========================================================================= */

// uncomment "OUTPUT_READABLE_QUATERNION" if you want to see the actual
// quaternion components in a [w, x, y, z] format (not best for parsing
// on a remote host such as Processing or something though)
// #define OUTPUT_READABLE_QUATERNION

// uncomment "OUTPUT_READABLE_EULER" if you want to see Euler angles
// (in degrees) calculated from the quaternions coming from the FIFO.
// Note that Euler angles suffer from gimbal lock (for more info, see
// http://en.wikipedia.org/wiki/Gimbal_lock)
//#define OUTPUT_READABLE_EULER

// uncomment "OUTPUT_READABLE_YAWPITCHROLL" if you want to see the yaw/
// pitch/roll angles (in degrees) calculated from the quaternions coming
// from the FIFO. Note this also requires gravity vector calculations.
// Also note that yaw/pitch/roll angles suffer from gimbal lock (for
// more info, see: http://en.wikipedia.org/wiki/Gimbal_lock)
// #define OUTPUT_READABLE_YAWPITCHROLL

// uncomment "OUTPUT_READABLE_REALACCEL" if you want to see acceleration
// components with gravity removed. This acceleration reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
// #define OUTPUT_READABLE_REALACCEL

// uncomment "OUTPUT_READABLE_WORLDACCEL" if you want to see acceleration
// components with gravity removed and adjusted for the world frame of
// reference (yaw is relative to initial orientation, since no magnetometer
// is present in this case). Could be quite handy in some cases.
#define OUTPUT_READABLE_WORLDACCEL

#define INTERRUPT_PIN PB8 // use pin 2 on Arduino Uno & most boards
#define LED_PIN PC13	  // (Arduino is 13, Teensy is 11, Teensy++ is 6)
bool blinkState = false;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;		// return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;	// expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;		// count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;		 // [w, x, y, z]         quaternion container
VectorInt16 aa;		 // [x, y, z]            accel sensor measurements
VectorInt16 gy;		 // [x, y, z]            gyro sensor measurements
VectorInt16 aaReal;  // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld; // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity; // [x, y, z]            gravity vector
float euler[3];		 // [psi, theta, phi]    Euler angle container
float ypr[3];		 // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false; // indicates whether MPU interrupt pin has gone high
void dmpDataReady()
{
	mpuInterrupt = true;
}

void AccelerometerModule::dmpDataReady(HardwareTimer *timer)
{
	uint32_t t1 = micros();
	uint32_t t2;
	mpuInterrupt = true;

	// // reset interrupt flag and get INT_STATUS byte
	// mpuInterrupt = false;
	mpuIntStatus = mpu.getIntStatus();

	// get current FIFO count
	fifoCount = mpu.getFIFOCount();
	// check for overflow (this should never happen unless our code is too inefficient)
	if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024)
	{
		// reset so we can continue cleanly
		mpu.resetFIFO();
		fifoCount = mpu.getFIFOCount();
		loggerSerial->println(F("FIFO overflow!"));

		// otherwise, check for DMP data ready interrupt (this should happen frequently)
	}
	else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT))
	{
		// wait for correct available data length, should be a VERY short wait
		while (fifoCount < packetSize)
			fifoCount = mpu.getFIFOCount();
		// loggerSerial->print("DENTRO ");
		// loggerSerial->print((String)fifoCount);
		// loggerSerial->print(" ");
		// loggerSerial->print((String)packetSize);
		// loggerSerial->print(" ");
		// loggerSerial->print((String)mpuIntStatus);
		// loggerSerial->print(" ");
		// loggerSerial->println((String)mpuInterrupt);
		// read a packet from FIFO
		mpu.getFIFOBytes(fifoBuffer, packetSize);
		// mpu.resetFIFO();
		// fifoCount = mpu.getFIFOCount();
		// track FIFO count here in case there is > 1 packet available
		// (this lets us immediately read more without waiting for an interrupt)
		fifoCount -= packetSize;
		// loggerSerial->print((String)fifoCount);
		// loggerSerial->print(" ");
		// loggerSerial->print((String)packetSize);
		// loggerSerial->print(" ");
		// loggerSerial->print((String)mpuIntStatus);
		// loggerSerial->print(" ");
		// loggerSerial->println((String)mpuInterrupt);

#ifdef OUTPUT_READABLE_QUATERNION
		// display quaternion values in easy matrix form: w x y z
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		loggerSerial->print("quat\t");
		loggerSerial->print(q.w);
		loggerSerial->print("\t");
		loggerSerial->print(q.x);
		loggerSerial->print("\t");
		loggerSerial->print(q.y);
		loggerSerial->print("\t");
		loggerSerial->println(q.z);
#endif

#ifdef OUTPUT_READABLE_EULER
		// display Euler angles in degrees
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		mpu.dmpGetEuler(euler, &q);
		loggerSerial->print("euler\t");
		loggerSerial->print(euler[0] * 180 / M_PI);
		loggerSerial->print("\t");
		loggerSerial->print(euler[1] * 180 / M_PI);
		loggerSerial->print("\t");
		loggerSerial->println(euler[2] * 180 / M_PI);
#endif

#ifdef OUTPUT_READABLE_YAWPITCHROLL
		// display Euler angles in degrees
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		mpu.dmpGetGravity(&gravity, &q);
		mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
		loggerSerial->print("ypr\t");
		loggerSerial->print(ypr[0] * 180 / M_PI);
		loggerSerial->print("\t");
		loggerSerial->print(ypr[1] * 180 / M_PI);
		loggerSerial->print("\t");
		loggerSerial->print(ypr[2] * 180 / M_PI);
		/*
            mpu.dmpGetAccel(&aa, fifoBuffer);
            loggerSerial->print("\tRaw Accl XYZ\t");
            loggerSerial->print(aa.x);
            loggerSerial->print("\t");
            loggerSerial->print(aa.y);
            loggerSerial->print("\t");
            loggerSerial->print(aa.z);
            mpu.dmpGetGyro(&gy, fifoBuffer);
            loggerSerial->print("\tRaw Gyro XYZ\t");
            loggerSerial->print(gy.x);
            loggerSerial->print("\t");
            loggerSerial->print(gy.y);
            loggerSerial->print("\t");
            loggerSerial->print(gy.z);
            */
		loggerSerial->println(" ");

#endif

#ifdef OUTPUT_READABLE_REALACCEL
		// display real acceleration, adjusted to remove gravity
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		mpu.dmpGetAccel(&aa, fifoBuffer);
		mpu.dmpGetGravity(&gravity, &q);
		mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
		loggerSerial->print("areal\t");
		loggerSerial->print(aaReal.x);
		loggerSerial->print("\t");
		loggerSerial->print(aaReal.y);
		loggerSerial->print("\t");
		loggerSerial->println(aaReal.z);
#endif

#ifdef OUTPUT_READABLE_WORLDACCEL
		// display initial world-frame acceleration, adjusted to remove gravity
		// and rotated based on known orientation from quaternion
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		mpu.dmpGetAccel(&aa, fifoBuffer);
		mpu.dmpGetGravity(&gravity, &q);
		mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
		mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
		loggerSerial->print("aworld\t");
		loggerSerial->print((float(aaWorld.x) / 8192.0) * 9.810);
		loggerSerial->print("\t");
		loggerSerial->print((float(aaWorld.y) / 8192.0) * 9.810);
		loggerSerial->print("\t");
		loggerSerial->println((float(aaWorld.z) / 8192.0) * 9.810);
#endif
	}

	loggerSerial->print("AX: ");
	t2 = micros();
	loggerSerial->println((String)(t2 - t1));
}

AccelerometerModule::AccelerometerModule()
{
	loggerSerial = NULL;
	accelLsbSensitiviy = 16384.0;
	gyroLsbSensitiviy = 131.0;
}

HardwareTimer *timerT = new HardwareTimer(TIM4);

void AccelerometerModule::Setup(AccelerometerModuleType moduleType, TwoWire *wire, int mpuAddress)
{
	AccelerometerModule::moduleType = moduleType;
	AccelerometerModule::mpuAddress = mpuAddress;
	axWire = wire;
	axWire->begin();
	axWire->setClock(400000);
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x6B);
	//Inicializa o MPU-6050
	axWire->write(0);
	axWire->endTransmission(true);

	// 0000 0101 CONFIG: Digital Low Pass Filter (DLPF) Configuration 10HZ - 0x05
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x1A);
	axWire->write(0x05);
	axWire->endTransmission(true);

	// 0000 0000 SMPLRT_DIV: ( Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV))
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x19);
	axWire->write(0x00);
	axWire->endTransmission(true);

	ConfigAccelerometer();
	ConfigGyroscope();
	EstimateError();
	Clean();
	ReadAccelerometer();
	CleanFilter();

	// Talvez tenhamos que colocar um cutoff para cada eixo
	// SetupBiasedFilter(0, 0.95);
	SetupBiasedFilter(biasAx, 0, 0.1);
	SetupBiasedFilter(biasAy, 0, 0.1);
	SetupBiasedFilter(biasAz, 0, 0.1);
	SetupBiasedFilter(biasJx, 0, 0.99);
	SetupBiasedFilter(biasJy, 0, 0.99);
	SetupBiasedFilter(biasJz, 0, 0.99);
	periodStart = micros();
	// bias.g = 0;
	// bias.cutoff = 0.95; //passível de ajuste - para o Jerk cutoff = 0.95
	// bias.cutoff = 0.1; //passível de ajuste

	// int16_t ax, ay, az;
	// int16_t gx, gy, gz;
	// Wire.begin();
	// Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties

	// // initialize device
	// loggerSerial->println("Initializing I2C devices...");
	// mpu.initialize();

	// // verify connection
	// loggerSerial->println("Testing device connections...");
	// loggerSerial->println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

	// while (true)
	// {
	// 	// read raw accel/gyro measurements from device
	// 	mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

	// 	// these methods (and a few others) are also available
	// 	//accelgyro.getAcceleration(&ax, &ay, &az);
	// 	//accelgyro.getRotation(&gx, &gy, &gz);

	// 	// display tab-separated accel/gyro x/y/z values
	// 	loggerSerial->print("a/g:\t");
	// 	loggerSerial->print(ax);
	// 	loggerSerial->print("\t");
	// 	loggerSerial->print(ay);
	// 	loggerSerial->print("\t");
	// 	loggerSerial->print(az);
	// 	loggerSerial->print("\t");
	// 	loggerSerial->print(gx);
	// 	loggerSerial->print("\t");
	// 	loggerSerial->print(gy);
	// 	loggerSerial->print("\t");
	// 	loggerSerial->println(gz);
	// }
	if (false)
	{
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
		Wire.begin();
		Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
							   // Wire.beginTransmission(mpuAddress);
							   // Wire.write(0x6B);
							   // // //Inicializa o MPU-6050
							   // Wire.write(0);
							   // Wire.endTransmission(true);
// #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
// 		Fastwire::setup(400, true);
#endif
		mpu.SetupLogger(loggerSerial);
		mpu.initialize();
		// pinMode(INTERRUPT_PIN, INPUT);
		loggerSerial->println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

		// verify connection
		loggerSerial->println(F("Testing device connections..."));
		loggerSerial->println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

		// wait for ready
		// loggerSerial->println(F("\nSend any character to begin DMP programming and demo: "));
		// while (loggerSerial->available() && loggerSerial->read())
		// 	; // empty buffer
		// while (!loggerSerial->available())
		// 	; // wait for data
		// while (loggerSerial->available() && loggerSerial->read())
		// 	; // empty buffer again

		// load and configure the DMP
		loggerSerial->println(F("Initializing DMP..."));
		devStatus = mpu.dmpInitialize();
		loggerSerial->println(F("FINISH DMP..."));
		// supply your own gyro offsets here, scaled for min sensitivity
		// mpu.setXGyroOffset(220);
		// mpu.setYGyroOffset(76);
		// mpu.setZGyroOffset(-85);
		// mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
		// mpu.setXGyroOffset(112.0);
		// mpu.setYGyroOffset(-2.0);
		// mpu.setZGyroOffset(-10.0);
		// mpu.setXAccelOffset(315.0);
		// mpu.setYAccelOffset(494.0);
		// mpu.setZAccelOffset(694.0);
		mpu.setXGyroOffset(105.0);
		mpu.setYGyroOffset(-5.0);
		mpu.setZGyroOffset(-5.0);
		mpu.setXAccelOffset(446.0);
		mpu.setYAccelOffset(287.0);
		mpu.setZAccelOffset(6148.0);
		// OFFSETs  446.0,   287.0,   6148.0,   105.0,    -5.0,    -5.0

		// make sure it worked (returns 0 if so)
		if (devStatus == 0)
		{
			loggerSerial->println(F("Calibrating MPU..."));
			// Calibration Time: generate offsets and calibrate our MPU6050
			mpu.CalibrateAccel(7);
			mpu.CalibrateGyro(7);
			mpu.PrintActiveOffsets();
			// turn on the DMP, now that it's ready
			loggerSerial->println(F("Enabling DMP..."));
			mpu.setDMPEnabled(true);

			// enable Arduino interrupt detection
			loggerSerial->print(F("Enabling interrupt detection (Arduino external interrupt "));
			// loggerSerial->print(digitalPinToInterrupt(INTERRUPT_PIN));
			loggerSerial->println(F(")..."));
			// attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);

			timerT->pause();
			timerT->setMode(3, TIMER_INPUT_CAPTURE_RISING);
			timerT->attachInterrupt(3, std::bind(AccelerometerModule::dmpDataReady,timerT));
			timerT->setPrescaleFactor(1);
			timerT->setOverflow(0x10000); // Max Period value to have the largest possible time to detect rising edge and avoid timer rollover
			// timerT->attachInterrupt(dmpDataReady);
			timerT->resume();

			mpuIntStatus = mpu.getIntStatus();

			// set our DMP Ready flag so the main loop() function knows it's okay to use it
			loggerSerial->println(F("DMP ready! Waiting for first interrupt..."));
			dmpReady = true;

			// get expected DMP packet size for later comparison
			packetSize = mpu.dmpGetFIFOPacketSize();
		}
		else
		{
			// ERROR!
			// 1 = initial memory load failed
			// 2 = DMP configuration updates failed
			// (if it's going to break, usually the code will be 1)
			loggerSerial->print(F("DMP Initialization failed (code "));
			loggerSerial->print(devStatus);
			loggerSerial->println(F(")"));
		}

		while (true)
		{
		}
		// 	while (true)
		// 	{
		// 		// if programming failed, don't try to do anything
		// 		if (!dmpReady)
		// 			return;

		// 		// wait for MPU interrupt or extra packet(s) available
		// 		while (!mpuInterrupt && fifoCount < packetSize)
		// 		{
		// 			if (mpuInterrupt && fifoCount < packetSize)
		// 			{
		// 				// try to get out of the infinite loop
		// 				fifoCount = mpu.getFIFOCount();
		// 			}
		// 			// other program behavior stuff here
		// 			// .
		// 			// .
		// 			// .
		// 			// if you are really paranoid you can frequently test in between other
		// 			// stuff to see if mpuInterrupt is true, and if so, "break;" from the
		// 			// while() loop to immediately process the MPU data
		// 			// .
		// 			// .
		// 			// .
		// 		}

		// 		// reset interrupt flag and get INT_STATUS byte
		// 		mpuInterrupt = false;
		// 		mpuIntStatus = mpu.getIntStatus();

		// 	loggerSerial->print("FORA ");
		// 	loggerSerial->print((String)fifoCount);
		// 	loggerSerial->print(" ");
		// 	loggerSerial->print((String)packetSize);
		// 	loggerSerial->print(" ");
		// 	loggerSerial->print((String)mpuIntStatus);
		// 	loggerSerial->print(" ");
		// 	loggerSerial->println((String)mpuInterrupt);

		// 		// get current FIFO count
		// 		fifoCount = mpu.getFIFOCount();
		// 		if (fifoCount < packetSize)
		// 		{
		// 			//Lets go back and wait for another interrupt. We shouldn't be here, we got an interrupt from another event
		// 			// This is blocking so don't do it   while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
		// 		}
		// 		// check for overflow (this should never happen unless our code is too inefficient)
		// 		else if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024)
		// 		{
		// 			// reset so we can continue cleanly
		// 			mpu.resetFIFO();
		// 			//  fifoCount = mpu.getFIFOCount();  // will be zero after reset no need to ask
		// 			loggerSerial->println(F("FIFO overflow!"));

		// 			// otherwise, check for DMP data ready interrupt (this should happen frequently)
		// 		}
		// 		else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT))
		// 		{

		// 			// read a packet from FIFO
		// 			while (fifoCount >= packetSize)
		// 			{ // Lets catch up to NOW, someone is using the dreaded delay()!
		// 				mpu.getFIFOBytes(fifoBuffer, packetSize);
		// 				// track FIFO count here in case there is > 1 packet available
		// 				// (this lets us immediately read more without waiting for an interrupt)
		// 				fifoCount -= packetSize;
		// 			}
		// #ifdef OUTPUT_READABLE_QUATERNION
		// 			// display quaternion values in easy matrix form: w x y z
		// 			mpu.dmpGetQuaternion(&q, fifoBuffer);
		// 			loggerSerial->print("quat\t");
		// 			loggerSerial->print(q.w);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->print(q.x);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->print(q.y);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->println(q.z);
		// #endif

		// #ifdef OUTPUT_READABLE_EULER
		// 			// display Euler angles in degrees
		// 			mpu.dmpGetQuaternion(&q, fifoBuffer);
		// 			mpu.dmpGetEuler(euler, &q);
		// 			loggerSerial->print("euler\t");
		// 			loggerSerial->print(euler[0] * 180 / M_PI);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->print(euler[1] * 180 / M_PI);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->println(euler[2] * 180 / M_PI);
		// #endif

		// #ifdef OUTPUT_READABLE_YAWPITCHROLL
		// 			// display Euler angles in degrees
		// 			mpu.dmpGetQuaternion(&q, fifoBuffer);
		// 			mpu.dmpGetGravity(&gravity, &q);
		// 			mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
		// 			loggerSerial->print("ypr\t");
		// 			loggerSerial->print(ypr[0] * 180 / M_PI);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->print(ypr[1] * 180 / M_PI);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->println(ypr[2] * 180 / M_PI);
		// #endif

		// #ifdef OUTPUT_READABLE_REALACCEL
		// 			// display real acceleration, adjusted to remove gravity
		// 			mpu.dmpGetQuaternion(&q, fifoBuffer);
		// 			mpu.dmpGetAccel(&aa, fifoBuffer);
		// 			mpu.dmpGetGravity(&gravity, &q);
		// 			mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
		// 			loggerSerial->print("areal\t");
		// 			loggerSerial->print(aaReal.x);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->print(aaReal.y);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->println(aaReal.z);
		// #endif

		// #ifdef OUTPUT_READABLE_WORLDACCEL
		// 			// display initial world-frame acceleration, adjusted to remove gravity
		// 			// and rotated based on known orientation from quaternion
		// 			mpu.dmpGetQuaternion(&q, fifoBuffer);
		// 			mpu.dmpGetAccel(&aa, fifoBuffer);
		// 			mpu.dmpGetGravity(&gravity, &q);
		// 			mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
		// 			mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
		// 			loggerSerial->print("aworld\t");
		// 			loggerSerial->print(aaWorld.x);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->print(aaWorld.y);
		// 			loggerSerial->print("\t");
		// 			loggerSerial->println(aaWorld.z);
		// #endif
		// 			// blink LED to indicate activity
		// 			// blinkState = !blinkState;
		// 			// digitalWrite(LED_PIN, blinkState);
		// 		}
		// 	}

		while (true)
		{
			// if programming failed, don't try to do anything
			if (!dmpReady)
				return;

			// wait for MPU interrupt or extra packet(s) available
			while (!mpuInterrupt && fifoCount < packetSize)
			{
				if (mpuInterrupt && fifoCount < packetSize)
				{
					// try to get out of the infinite loop
					fifoCount = mpu.getFIFOCount();
				}
				// other program behavior stuff here
				// .
				// .
				// .
				// if you are really paranoid you can frequently test in between other
				// stuff to see if mpuInterrupt is true, and if so, "break;" from the
				// while() loop to immediately process the MPU data
				// .
				// .
				// .
				if (mpuInterrupt == true)
					break;
			}

			// reset interrupt flag and get INT_STATUS byte
			mpuInterrupt = false;
			mpuIntStatus = mpu.getIntStatus();

			// get current FIFO count
			fifoCount = mpu.getFIFOCount();

			// check for overflow (this should never happen unless our code is too inefficient)
			if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024)
			{
				// reset so we can continue cleanly
				mpu.resetFIFO();
				fifoCount = mpu.getFIFOCount();
				loggerSerial->println(F("FIFO overflow!"));

				// otherwise, check for DMP data ready interrupt (this should happen frequently)
			}
			else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT))
			{
				// wait for correct available data length, should be a VERY short wait
				while (fifoCount < packetSize)
					fifoCount = mpu.getFIFOCount();
				loggerSerial->print("DENTRO ");
				loggerSerial->print((String)fifoCount);
				loggerSerial->print(" ");
				loggerSerial->print((String)packetSize);
				loggerSerial->print(" ");
				loggerSerial->print((String)mpuIntStatus);
				loggerSerial->print(" ");
				loggerSerial->println((String)mpuInterrupt);
				// read a packet from FIFO
				mpu.getFIFOBytes(fifoBuffer, packetSize);
				mpu.resetFIFO();
				fifoCount = mpu.getFIFOCount();
				// track FIFO count here in case there is > 1 packet available
				// (this lets us immediately read more without waiting for an interrupt)
				// fifoCount -= packetSize;
				loggerSerial->print((String)fifoCount);
				loggerSerial->print(" ");
				loggerSerial->print((String)packetSize);
				loggerSerial->print(" ");
				loggerSerial->print((String)mpuIntStatus);
				loggerSerial->print(" ");
				loggerSerial->println((String)mpuInterrupt);

#ifdef OUTPUT_READABLE_QUATERNION
				// display quaternion values in easy matrix form: w x y z
				mpu.dmpGetQuaternion(&q, fifoBuffer);
				loggerSerial->print("quat\t");
				loggerSerial->print(q.w);
				loggerSerial->print("\t");
				loggerSerial->print(q.x);
				loggerSerial->print("\t");
				loggerSerial->print(q.y);
				loggerSerial->print("\t");
				loggerSerial->println(q.z);
#endif

#ifdef OUTPUT_READABLE_EULER
				// display Euler angles in degrees
				mpu.dmpGetQuaternion(&q, fifoBuffer);
				mpu.dmpGetEuler(euler, &q);
				loggerSerial->print("euler\t");
				loggerSerial->print(euler[0] * 180 / M_PI);
				loggerSerial->print("\t");
				loggerSerial->print(euler[1] * 180 / M_PI);
				loggerSerial->print("\t");
				loggerSerial->println(euler[2] * 180 / M_PI);
#endif

#ifdef OUTPUT_READABLE_YAWPITCHROLL
				// display Euler angles in degrees
				mpu.dmpGetQuaternion(&q, fifoBuffer);
				mpu.dmpGetGravity(&gravity, &q);
				mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
				loggerSerial->print("ypr\t");
				loggerSerial->print(ypr[0] * 180 / M_PI);
				loggerSerial->print("\t");
				loggerSerial->print(ypr[1] * 180 / M_PI);
				loggerSerial->print("\t");
				loggerSerial->print(ypr[2] * 180 / M_PI);
				/*
            mpu.dmpGetAccel(&aa, fifoBuffer);
            loggerSerial->print("\tRaw Accl XYZ\t");
            loggerSerial->print(aa.x);
            loggerSerial->print("\t");
            loggerSerial->print(aa.y);
            loggerSerial->print("\t");
            loggerSerial->print(aa.z);
            mpu.dmpGetGyro(&gy, fifoBuffer);
            loggerSerial->print("\tRaw Gyro XYZ\t");
            loggerSerial->print(gy.x);
            loggerSerial->print("\t");
            loggerSerial->print(gy.y);
            loggerSerial->print("\t");
            loggerSerial->print(gy.z);
            */
				loggerSerial->println(" ");

#endif

#ifdef OUTPUT_READABLE_REALACCEL
				// display real acceleration, adjusted to remove gravity
				mpu.dmpGetQuaternion(&q, fifoBuffer);
				mpu.dmpGetAccel(&aa, fifoBuffer);
				mpu.dmpGetGravity(&gravity, &q);
				mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
				loggerSerial->print("areal\t");
				loggerSerial->print(aaReal.x);
				loggerSerial->print("\t");
				loggerSerial->print(aaReal.y);
				loggerSerial->print("\t");
				loggerSerial->println(aaReal.z);
#endif

#ifdef OUTPUT_READABLE_WORLDACCEL
				// display initial world-frame acceleration, adjusted to remove gravity
				// and rotated based on known orientation from quaternion
				mpu.dmpGetQuaternion(&q, fifoBuffer);
				mpu.dmpGetAccel(&aa, fifoBuffer);
				mpu.dmpGetGravity(&gravity, &q);
				mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
				mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
				loggerSerial->print("aworld\t");
				loggerSerial->print(aaWorld.x);
				loggerSerial->print("\t");
				loggerSerial->print(aaWorld.y);
				loggerSerial->print("\t");
				loggerSerial->println(aaWorld.z);
#endif
			}
		}
	}
}

void AccelerometerModule::SetupLogger(LoggerSerial *loggerSerial)
{
	if (loggerSerial == NULL)
	{
		AccelerometerModule::loggerSerial = new LoggerSerial();
	}
	else
		AccelerometerModule::loggerSerial = loggerSerial;
}

void AccelerometerModule::ConfigAccelerometer(uint8_t AFS_SEL)
{
	// Configure Accelerometer Sensitivity - Full Scale Range (default +/- 2g)
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x1C); //Talk to the ACCEL_CONFIG register (1C hex)
	switch (AFS_SEL)
	{
	case 3:
		axWire->write(0x18); // Set the register bits as 00011000 (+/- 16g full scale range)
		accelLsbSensitiviy = 2048;
		break;
	case 2:
		axWire->write(0x10);	   // Set the register bits as 00010000 (+/- 8g full scale range)
		accelLsbSensitiviy = 4096; // 4096 LSB/g
		break;
	case 1:
		axWire->write(0x08); // Set the register bits as 00001000 (+/- 4g full scale range)
		accelLsbSensitiviy = 8192;
		break;
	case 0:							// +/ 250 deg/s
	default:						// (default +/- 250deg/s)
		axWire->write(0x00);		// Set the register bits as 00000000 (+/- 2g full scale range)
		accelLsbSensitiviy = 16384; // 131 LSB/°/s
		break;
	}
	axWire->endTransmission(true);
}

void AccelerometerModule::ConfigGyroscope(uint8_t FS_SEL)
{
	// Configure Gyro Sensitivity - Full Scale Range
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x1B); // Talk to the GYRO_CONFIG register (1B hex)
	switch (FS_SEL)
	{
	case 3:
		axWire->write(0x18); // Set the register bits as 00011000 (2000deg/s full scale)
		gyroLsbSensitiviy = 16.4;
		break;
	case 2:
		axWire->write(0x10); // Set the register bits as 00010000 (1000deg/s full scale)
		gyroLsbSensitiviy = 32.8;
		break;
	case 1:
		axWire->write(0x08); // Set the register bits as 00001000 (500deg/s full scale)
		gyroLsbSensitiviy = 65.5;
		break;
	case 0:						   // +/ 250 deg/s
	default:					   // (default +/- 250deg/s)
		axWire->write(0x00);	   // Set the register bits as 00000000 (250deg/s full scale)
		gyroLsbSensitiviy = 131.0; // 131 LSB/°/s
		break;
	}
	axWire->endTransmission(true);
}

void AccelerometerModule::EstimateError()
{
	int c = 0, n = 700;
	int ax, ay, az, gx, gy, gz;

	// We can call this funtion in the setup section to calculate the accelerometer and gyro data error. From here we will get the error values used in the above equations printed on the Serial Monitor.
	// Note that we should place the IMU flat in order to get the proper values, so that we then can the correct values
	// Read accelerometer values n times
	errorValues.az = errorValues.gx = errorValues.gy = errorValues.gz = errorValues.ay = errorValues.ax = 0;
	while (c < n)
	{
		// loggerSerial->println((String) c);
		axWire->beginTransmission(mpuAddress);
		axWire->write(0x3B); //Self-test?
		axWire->endTransmission(false);
		axWire->requestFrom(mpuAddress, 6, true);
		ax = (axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY;
		ay = (axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY;
		az = (axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY;
		// Sum all readings
		//errorValues.ax = errorValues.ax + ((atan((ay) / sqrt(pow((ax), 2) + pow((az), 2))) * 180 / PI));
		//errorValues.ay = errorValues.ay + ((atan(-1 * (ax) / sqrt(pow((ay), 2) + pow((az), 2))) * 180 / PI));
		errorValues.ax += ax;
		errorValues.ay += ay;
		errorValues.az += az;
		c++;
	}
	//Divide the sum by n to get the error value
	errorValues.ax /= n;
	errorValues.ay /= n;
	errorValues.az /= n;
	c = 0;
	// Read gyro values n times
	while (c < n)
	{
		axWire->beginTransmission(mpuAddress);
		axWire->write(0x43);
		axWire->endTransmission(false);
		axWire->requestFrom(mpuAddress, 6, true);
		gx = axWire->read() << 8 | axWire->read();
		gy = axWire->read() << 8 | axWire->read();
		gz = axWire->read() << 8 | axWire->read();
		// Sum all readings
		errorValues.gx += (gx / GYRO_LSB_SENSITIVITY);
		errorValues.gy += (gy / GYRO_LSB_SENSITIVITY);
		errorValues.gz += (gz / GYRO_LSB_SENSITIVITY);
		c++;
	}
	//Divide the sum by n to get the error value
	errorValues.gx /= n;
	errorValues.gy /= n;
	errorValues.gz /= n;
}

void AccelerometerModule::ReadAccelerometer()
{
	lastAxcelRead = micros();
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
	axWire->endTransmission(false);
	//Solicita os dados do sensor
	axWire->requestFrom(mpuAddress, 6, true); //(device, number of bytes,send stop)
	//Armazena o valor dos sensores nas variaveis correspondentes
	data.ax = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY - errorValues.ax); //0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	data.ay = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY - errorValues.ay); //0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	data.az = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY - errorValues.az); //0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

	// data.ax = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY); //0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	// data.ay = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY); //0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	// data.az = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY); //0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

	data.ax *= GRAVITY;
	data.ay *= GRAVITY;
	data.az *= GRAVITY;
}

void AccelerometerModule::meanJerk()
{
	int c;
	double ax1, ax2, ay1, ay2, az1, az2;
	double jx, jy, jz;
	nSamplesJerk = 0;
	// loggerSerial->println("READING AXCEL ONLY - S");
	ReadAccelerometer();
	// loggerSerial->println("READING AXCEL ONLY - E");
	ax1 = data.ax;
	ay1 = data.ay;
	az1 = data.az;

	for (c = 1; c < 400; c++)
	{
		// loggerSerial->println("LOOP 1");
		while (micros() - lastAxcelRead < 2500)
			; //Block until the right moment to read at 400hz
		// loggerSerial->println("LOOP 2");
		ReadAccelerometer();

		// ax2 = 9.81 * data.ax; //Conversion from Gs to m/s^2
		// ay2 = 9.81 * data.ay; //Conversion from Gs to m/s^2
		// az2 = 9.81 * data.az; //Conversion from Gs to m/s^2

		data.jx = (ax2 - ax1) / 0.0025;
		data.jy = (ay2 - ay1) / 0.0025;
		data.jz = (az2 - az1) / 0.0025;

		data.meanJerkX = (data.meanJerkX * nSamplesJerk + data.jx) / (nSamplesJerk + 1);
		data.meanJerkY = (data.meanJerkY * nSamplesJerk + data.jy) / (nSamplesJerk + 1);
		data.meanJerkZ = (data.meanJerkZ * nSamplesJerk + data.jz) / (nSamplesJerk + 1);
		nSamplesJerk++;

		ax1 = ax2;
		ay1 = ay2;
		az1 = az2;
	}
	// loggerSerial->println("MEAN JERK END");
}

void AccelerometerModule::CleanFilter()
{
	SetupBiasedFilter(biasAx, 0, biasAx.cutoff);
	SetupBiasedFilter(biasAy, 0, biasAy.cutoff);
	SetupBiasedFilter(biasAz, 0, biasAz.cutoff);
	SetupBiasedFilter(biasJx, 0, biasJy.cutoff);
	SetupBiasedFilter(biasJy, 0, biasJy.cutoff);
	SetupBiasedFilter(biasJz, 0, biasJy.cutoff);
}

void AccelerometerModule::SetupBiasedFilter(BiasedFilter &filter, double g, double cutoff)
{
	filter.g = g;
	filter.cutoff = cutoff;
}

void AccelerometerModule::Filter(void)
{

	data.axClean = BiasedFiltering(biasAx, data.ax);
	data.ayClean = BiasedFiltering(biasAy, data.ay);
	data.azClean = BiasedFiltering(biasAz, data.az);
	data.axCleanJ = BiasedFiltering(biasJx, data.ax);
	data.ayCleanJ = BiasedFiltering(biasJy, data.ay);
	data.azCleanJ = BiasedFiltering(biasJz, data.az);
	// data.ay = BiasedFiltering(biasJy, data.ay);
}

double AccelerometerModule::BiasedFiltering(BiasedFilter &filter, double acc)
{
	double oldG = filter.g;
	filter.g = (1 - filter.cutoff) * filter.g + filter.cutoff * acc;
	// return (acc - filter.g) / (1 - filter.cutoff + oldG * (filter.cutoff - 1));
	return acc - filter.g;
}

void AccelerometerModule::RealTimeLongitudinalJerk()
{
	double ax1 = data.axCleanJ, ax2;
	double ay1 = data.ayCleanJ, ay2;
	double az1 = data.azCleanJ, az2;
	// double jy;
	double T = ((double)micros() - lastAxcelRead) / 1000000.0;
	unsigned long timeElapsed;

	ReadAccelerometer();
	// data.ay = FilterLongitudinalAceleration(data.ay);
	//ay2 = data.ay; //Conversion from Gs to m/s^2
	//jy = (ay2 - ay1) /T;
	//data.meanJerkY = (ay2 - ay1) / T;
	//data.meanJerkY = (data.meanJerkY * nSamplesJerk + jy) / (nSamplesJerk + 1);
	// nSamplesJerk++;

	Filter();

	CalculateOverallAccel();
	// (float(aaWorld.x) / 8192.0) * 9.810
	//loggerSerial->print("aa:");
	//loggerSerial->print(" ");
	//loggerSerial->print(data.ax);
	//loggerSerial->print(" ");
	//loggerSerial->println(BiasedFiltering(data.ay));
	ax2 = data.axCleanJ;
	ay2 = data.ayCleanJ;
	az2 = data.azCleanJ;
	data.jx = (ax2 - ax1) / T;
	data.jy = (ay2 - ay1) / T;
	data.jz = (az2 - az1) / T;

	if (data.jx >= 0.4) //
		data.jxCount0++;
	if (data.jy >= 0.4) //
		data.jyCount0++;
	if (data.jz >= 0.4) //
		data.jzCount0++;
	// loggerSerial->print("ax:");
	// loggerSerial->print(data.axClean);
	// loggerSerial->print("\tay:");
	// loggerSerial->print(data.ayClean);
	// loggerSerial->print("\taz:");
	// loggerSerial->print(data.azClean);
	// loggerSerial->print("\tavClean:");
	// loggerSerial->print(data.avClean);
	// loggerSerial->print("\tav:");
	// loggerSerial->print(data.av);
	// loggerSerial->print(" ");
	/*timeElapsed = micros() - periodStart;
	if(timeElapsed > 1000000)//1s
	{
		//CleanFilter();
		periodStart = micros();
	}*/
	// loggerSerial->print("\tjx:");
	// loggerSerial->print(jx);
	// loggerSerial->print("\tjy:");
	// loggerSerial->print(jy);
	// loggerSerial->print("\tjz:");
	// loggerSerial->print(jz);
	// loggerSerial->println(data.meanJerkY);
	// loggerSerial->println(" ");
}

void AccelerometerModule::Clean(void)
{
	nSamplesJerk = 0;
	data.ax = 0;
	data.ay = 0;
	data.az = 0;
	data.jx = 0;
	data.jy = 0;
	data.jz = 0;
	data.meanJerkX = 0;
	data.meanJerkY = 0;
	data.meanJerkZ = 0;
	CleanCounts();
}

void AccelerometerModule::CleanCounts(void)
{
	data.jxCount0 = 0;
	data.jyCount0 = 0;
	data.jzCount0 = 0;
}

void AccelerometerModule::CalculateMeanJerk(void)
{
	double ax1, ay1, az1;

	ax1 = data.ax;
	ay1 = data.ay;
	az1 = data.az;

	ReadAccelerometer();

	data.jx = (data.ax - ax1) / 0.0025;
	data.jy = (data.ay - ay1) / 0.0025;
	data.jz = (data.az - az1) / 0.0025;

	data.meanJerkX = (data.meanJerkX * nSamplesJerk + data.jx) / (nSamplesJerk + 1);
	data.meanJerkY = (data.meanJerkY * nSamplesJerk + data.jy) / (nSamplesJerk + 1);
	data.meanJerkZ = (data.meanJerkZ * nSamplesJerk + data.jz) / (nSamplesJerk + 1);
	nSamplesJerk++;
}

void AccelerometerModule::CalculateOverallAccel(void)
{
	data.av = pow(data.ax * data.ax + data.ay * data.ay + data.az * data.az, 0.5);
	data.avClean = pow(data.axClean * data.axClean + data.ayClean * data.ayClean + data.azClean * data.azClean, 0.5);
}

AxcelData AccelerometerModule::Read(void)
{
	unsigned long a, b;
	a = micros();
	axWire->beginTransmission(mpuAddress);
	axWire->write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
	axWire->endTransmission(false);
	//Solicita os dados do sensor
	axWire->requestFrom(mpuAddress, 14, true); //(device, number of bytes,send stop)
	//Armazena o valor dos sensores nas variaveis correspondentes
	data.ax = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY - errorValues.ax); //0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	data.ay = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY - errorValues.ay); //0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	data.az = ((axWire->read() << 8 | axWire->read()) / ACCEL_LSB_SENSITIVITY - errorValues.az); //0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	data.T = ((int)(axWire->read() << 8 | axWire->read())) / 340.0 + 36.53;						 //0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	data.gx = ((axWire->read() << 8 | axWire->read()) / GYRO_LSB_SENSITIVITY - errorValues.gx);  //0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	data.gy = ((axWire->read() << 8 | axWire->read()) / GYRO_LSB_SENSITIVITY - errorValues.gy);  //0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
	data.gz = ((axWire->read() << 8 | axWire->read()) / GYRO_LSB_SENSITIVITY - errorValues.gz);  //0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
	b = micros();

	return data;
}