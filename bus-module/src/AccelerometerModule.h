#ifndef ACCELEROMETERMODULE_H_
#define ACCELEROMETERMODULE_H_

#include <Arduino.h>
#include <Wire.h>
#include <math.h>
#include "CommunicationModule/LoggerSerial.h"

#define ACCEL_LSB_SENSITIVITY 4096.0
#define GYRO_LSB_SENSITIVITY 32.8
#define GRAVITY 9.810

typedef enum
{
	ACCELMOD_MPU6050,
} AccelerometerModuleType;

typedef struct
{
	uint16_t jxCount0;
	uint16_t jyCount0;
	uint16_t jzCount0;
} AxcelSendData;

typedef struct
{
	double ax, ay, az, gx, gy, gz;
	double av;								   // overall whole-body vibration acceleration
	double axClean, ayClean, azClean, avClean; //filtered accelerations
	double axCleanJ, ayCleanJ, azCleanJ;	   //filtered accelerations for Jerk
	double jx, jy, jz;
	double meanJerkX, meanJerkY, meanJerkZ;
	uint16_t jxCount0;
	uint16_t jyCount0;
	uint16_t jzCount0;
	float T;
} AxcelData;

typedef struct
{
	double g;
	double cutoff;
} BiasedFilter;

class AccelerometerModule
{
private:
	static LoggerSerial *loggerSerial;

	AccelerometerModuleType moduleType;

public:
	TwoWire *axWire;

	int mpuAddress;
	AxcelData data;
	AxcelData errorValues;
	BiasedFilter biasAx, biasAy, biasAz;
	BiasedFilter biasJx, biasJy, biasJz;
	double accelLsbSensitiviy;
	double gyroLsbSensitiviy;

	unsigned long lastAxcelRead, periodStart;
	int nSamplesJerk;

	static void dmpDataReady(HardwareTimer *timer);

	AccelerometerModule();
	void Setup(AccelerometerModuleType moduleType, TwoWire *wire, int mpuAddress = 0x68);
	void SetupLogger(LoggerSerial *loggerSerial);
	void ConfigAccelerometer(uint8_t AFS_SEL = 2);
	void ConfigGyroscope(uint8_t FS_SEL = 2);
	void EstimateError();
	void ReadAccelerometer();
	void meanJerk();
	void RealTimeLongitudinalJerk();
	void Clean(void);
	void CleanCounts(void);
	void CalculateMeanJerk(void);
	void SetupBiasedFilter(BiasedFilter &filter, double g, double cutoff);
	void CleanFilter(void);
	double BiasedFiltering(BiasedFilter &filter, double acc);
	void Filter(void);
	void CalculateOverallAccel(void); // av
	AxcelData Read(void);
	// AxcelData Ge(void);
};
#endif /* ACCELEROMETERMODULE_H_ */