#include "BusModule.h"

LoggerSerial *BusModule::loggerSerial = NULL;

BusModuleType BusModule::moduleType;

TemperatureModule BusModule::temperatureModule;
NoiseModule BusModule::noiseModule;
GpsModule BusModule::gpsModule;
CommunicationModule BusModule::communicationModule;
AccelerometerModule BusModule::accelerometerModule;

BusData BusModule::busData;
DataList *BusModule::dataList = new DataList();

uint32_t BusModule::nPackets = 0;

uint32_t BusModule::pinLedSucess = 0;
uint32_t BusModule::pinLedError = 0;
uint32_t BusModule::pinCurrentLed = 0;
uint32_t BusModule::toggle = 0;
// HardwareTimer *BusModule::TimerBlinkLed = new HardwareTimer(TIM4);
HardwareTimer *BusModule::TimerBlinkLed = NULL;
// Timer responsible for saving current BusData to the output buffer vector,
// Sending the latest BusData to the Master
// Checking for ACKNOWNLEDGMENTs in the communicationModule
// Removing the ACKNOWLEDGED packets from the output buffer vector
HardwareTimer *BusModule::TimerData = new HardwareTimer(TIM1);
HardwareTimer *BusModule::TimerAccel = new HardwareTimer(TIM3);
HardwareTimer *BusModule::TimerNoise = new HardwareTimer(TIM2);

uint32_t BusModule::routineTime = 30;
uint32_t BusModule::msRoutineTime;
uint8_t BusModule::sendRetries = 2;
uint8_t BusModule::tempRetries = 2;
uint8_t BusModule::gpsRetries = 1;
uint8_t BusModule::accelSampling = 500;
uint8_t BusModule::noiseSampling = 1000;

BusModule::BusModule()
{
	loggerSerial = NULL;
	pinLedSucess = 0;
	pinLedError = 0;
	pinCurrentLed = 0;
	toggle = 0;
	// dataList->nextNode = NULL;
	SetErrorValues();
}

void BusModule::Setup(BusModuleType moduleType, uint32_t pinLedSucess,
					  uint32_t pinLedError, TwoWire *accelWire,
					  SomeSerial *gpsSerial, SomeSerial *transparentSerial,
					  SomeSerial *commandSerial)
{
	BusModule::moduleType = moduleType;

	temperatureModule.SetupLogger(loggerSerial);
	noiseModule.SetupLogger(loggerSerial);
	communicationModule.SetupLogger(loggerSerial);
	gpsModule.SetupLogger(loggerSerial);
	accelerometerModule.SetupLogger(loggerSerial);
	// loggerSerial->println("DEPOIS DOS LOGGERS");

	// TODO: Check if the serial are not null

	if (moduleType == BUSMOD_STM32F103C8_128K)
	{
		// temperatureModule.Setup(TEMPSENSOR_DHT11, PB4, DHT11_SAMPLINGTIME, 1);
		temperatureModule.Setup(TEMPSENSOR_DHT11, DHT11_PIN, DHT11_SAMPLINGTIME, 1);
		// noiseModule.Setup(NOISESENSOR_KY038, PA4, PA5);
		noiseModule.Setup(NOISESENSOR_KY038, MIC_APIN, MIC_DPIN);

		communicationModule.Setup(COMMMOD_RADIOENGELORAMESH, BUS_MODULE_PAYLOAD_SIZE, 500, 1,
								  RADIOENGELORAMESH_DEFAULT_DEVICE_ID,
								  transparentSerial, LORAMESH_TRANS_BAUDRATE);
		// loggerSerial->println("ACELL B");
		accelerometerModule.Setup(ACCELMOD_MPU6050, accelWire);
		// loggerSerial->println("ACELL A");
	}
	else
	{
		// #ifdef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
		// 		gpsSerial = new SomeSerial(NEO7M_TX_PIN, NEO7M_RX_PIN);
		// 		commandSerial = new SomeSerial(LORAMESH_TX_COMM_PIN, LORAMESH_RX_COMM_PIN);
		// 		transparentSerial = new SomeSerial(LORAMESH_TX_TRANS_PIN, LORAMESH_RX_TRANS_PIN);
		// #endif

		// loggerSerial->println("ENTROU???");
		temperatureModule.Setup(TEMPSENSOR_DHT11, DHT11_PIN, DHT11_SAMPLINGTIME, 1);
		noiseModule.Setup(NOISESENSOR_KY038, MIC_APIN, MIC_DPIN);
		communicationModule.Setup(COMMMOD_RADIOENGELORAMESH, BUS_MODULE_PAYLOAD_SIZE, 5000, 1,
								  commandSerial, LORAMESH_COMM_BAUDRATE,
								  transparentSerial, LORAMESH_TRANS_BAUDRATE);
	}

	gpsModule.Setup(GPSMOD_NEO7M, 2000, 5, gpsSerial, NEO7M_BAUDRATE);

	BusModule::pinLedSucess = pinLedSucess;
	BusModule::pinLedError = pinLedError;
	BusModule::toggle = 0;

	// SetupBlinkLedTimer(TimerBlinkLed);
	pinMode(pinLedSucess, OUTPUT);
	pinMode(pinLedError, OUTPUT);
}

void BusModule::SetupLogger(LoggerSerial *loggerSerial)
{
	if (loggerSerial == NULL)
	{
		BusModule::loggerSerial = new LoggerSerial();
	}
	else
		BusModule::loggerSerial = loggerSerial;
}

void BusModule::SetupTimers(uint32_t routineTime, uint8_t sendRetries, uint8_t tempRetries,
							uint8_t gpsRetries, uint8_t accelSampling, uint8_t noiseSampling)
{
	BusModule::routineTime = routineTime;
	BusModule::sendRetries = sendRetries;
	BusModule::tempRetries = tempRetries;
	BusModule::gpsRetries = gpsRetries;
	BusModule::accelSampling = accelSampling;
	BusModule::noiseSampling = noiseSampling;
	SetupDataTimer(TimerData);
	SetupAccelTimer(TimerAccel);
	// SetupNoiseTimer(TimerNoise);
}

void BusModule::SetupTimerGpsOnly(uint32_t routineTime, uint8_t sendRetries, uint8_t gpsRetries)
{
	BusModule::routineTime = routineTime;
	BusModule::sendRetries = sendRetries;
	BusModule::gpsRetries = gpsRetries;
	SetupDataTimer(TimerData, true, false, false);
}
void BusModule::SetupTimerAxcelOnly(uint8_t accelSampling)
{
	BusModule::accelSampling = accelSampling;
	SetupAccelTimer(TimerAccel);
}

void BusModule::SetupTimerNoiseOnly(uint8_t noiseSampling)
{
	BusModule::noiseSampling = noiseSampling;
	SetupNoiseTimer(TimerNoise);
}

void BusModule::ExecuteRoutine(void)
{
	uint8_t *currentPayload = NULL;

	loggerSerial->println("STARTING ROUTINE");

	loggerSerial->println("TEMP READING");
	temperatureModule.Read();
	loggerSerial->println("NOISE READING");
	noiseModule.Read();
	loggerSerial->println("GPS READING");
	gpsModule.Read();

	busData.nmeaData = gpsModule.GetNmeaRmc();

	loggerSerial->println("GPS DONE");

	busData.temperature = temperatureModule.temperature;
	busData.humidity = temperatureModule.humidity;
	busData.noiseData.maxNoiseDb = noiseModule.data.maxNoiseDb;
	busData.noiseData.noiseCount[0] = noiseModule.data.noiseCount[0];
	busData.noiseData.noiseCount[1] = noiseModule.data.noiseCount[1];
	busData.noiseData.noiseCount[2] = noiseModule.data.noiseCount[2];
	busData.noiseData.noiseCount[3] = noiseModule.data.noiseCount[3];
	busData.noiseData.noiseCount[4] = noiseModule.data.noiseCount[4];
	noiseModule.Clean();
	// busData.maxNoiseDb = noiseModule.GetMaxNoiseDb(true);

	loggerSerial->println("SENSORS DONE");
	printBusSensorsData(NULL);

	loggerSerial->println("CONVERTING TO PAYLOAD");
	currentPayload = ConvertToBytesPayload(busData);

	loggerSerial->println("SENDING DATA");
	communicationModule.SendData(currentPayload);

	loggerSerial->println("PACKAGE SENT");
	if (communicationModule.communicationSuccess == true)
	{
		loggerSerial->println("COMMUNICATION SUCCESS");
		BlinkLed(pinLedSucess);
	}
	else
	{
		loggerSerial->println("COMMUNICATION ERROR");
		BlinkLed(pinLedError);
	}

	if (currentPayload != NULL)
		free(currentPayload);
	delay(5000);
}

void BusModule::SetErrorValues(void)
{
	busData.humidity = 0xFF;
	busData.temperature = 0xFF;
	busData.noiseData;
	// busData.accelerometerData;
	busData.nmeaData.day = 0xFF;
	busData.nmeaData.month = 0xFF;
	busData.nmeaData.year = 0xFF;
	busData.nmeaData.hours = 0xFF;
	busData.nmeaData.minutes = 0xFF;
	busData.nmeaData.seconds = 0xFF;
	busData.nmeaData.status = 0xFF;
	busData.nmeaData.latitude;
	busData.nmeaData.longitude;
	busData.nmeaData.speed;
}

void BusModule::SetErrorValues(BusData *busData)
{
	busData->humidity = 0xFF;
	busData->temperature = 0xFF;
	busData->noiseData;
	// busData->accelerometerData;
	busData->nmeaData.day = 0xFF;
	busData->nmeaData.month = 0xFF;
	busData->nmeaData.year = 0xFF;
	busData->nmeaData.hours = 0xFF;
	busData->nmeaData.minutes = 0xFF;
	busData->nmeaData.seconds = 0xFF;
	busData->nmeaData.status = 0xFF;
	busData->nmeaData.latitude;
	busData->nmeaData.longitude;
	busData->nmeaData.speed;
}

uint8_t *BusModule::ConvertToBytesPayload(BusData data)
{
	uint8_t *payload;
	uint8_t bytes[sizeof(float)];
	uint8_t bytes16[sizeof(uint16_t)];
	int c = 0;
	payload = (uint8_t *)malloc(sizeof(uint8_t) * BUS_MODULE_PAYLOAD_SIZE);
	// DAY;
	payload[c++] = (uint8_t)data.nmeaData.day;
	// MONTH;
	payload[c++] = (uint8_t)data.nmeaData.month;
	// YEAR;
	payload[c++] = (uint8_t)data.nmeaData.year;
	// HOURS;
	payload[c++] = (uint8_t)data.nmeaData.hours;
	// MINUTES;
	payload[c++] = (uint8_t)data.nmeaData.minutes;
	// SECONDS;
	payload[c++] = (uint8_t)data.nmeaData.seconds;
	// STATUS;
	payload[c++] = (uint8_t)data.nmeaData.status;

	// LATITUDE;
	*(float *)(bytes) = data.nmeaData.latitude;
	payload[c++] = bytes[0];
	payload[c++] = bytes[1];
	payload[c++] = bytes[2];
	payload[c++] = bytes[3];

	// LONGITUDE;
	*(float *)(bytes) = data.nmeaData.longitude;
	payload[c++] = bytes[0];
	payload[c++] = bytes[1];
	payload[c++] = bytes[2];
	payload[c++] = bytes[3];

	// SPEED;
	*(float *)(bytes) = data.nmeaData.speed;
	payload[c++] = bytes[0];
	payload[c++] = bytes[1];
	payload[c++] = bytes[2];
	payload[c++] = bytes[3];

	// TEMP;
	payload[c++] = (uint8_t)data.temperature;
	// HUMIDITY;
	payload[c++] = (uint8_t)data.humidity;

	// // NOISERAW;
	// *(float *)(bytes) = data.meanNoiseRaw;
	// payload[c++] = bytes[0];
	// payload[c++] = bytes[1];
	// payload[c++] = bytes[2];
	// payload[c++] = bytes[3];

	// MAXNOISEDB;
	*(float *)(bytes) = data.noiseData.maxNoiseDb;
	payload[c++] = bytes[0];
	payload[c++] = bytes[1];
	payload[c++] = bytes[2];
	payload[c++] = bytes[3];

	// NOISECOUNT0;
	*(uint16_t *)(bytes16) = data.noiseData.noiseCount[0];
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// NOISECOUNT1;
	*(uint16_t *)(bytes16) = data.noiseData.noiseCount[1];
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// NOISECOUNT2;
	*(uint16_t *)(bytes16) = data.noiseData.noiseCount[2];
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// NOISECOUNT3;
	*(uint16_t *)(bytes16) = data.noiseData.noiseCount[3];
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// NOISECOUNT4;
	*(uint16_t *)(bytes16) = data.noiseData.noiseCount[4];
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// JXCOUNT0;
	*(uint16_t *)(bytes16) = data.accelerometerData.jxCount0;
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// JYCOUNT0;
	*(uint16_t *)(bytes16) = data.accelerometerData.jyCount0;
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	// JZCOUNT0;
	*(uint16_t *)(bytes16) = data.accelerometerData.jzCount0;
	payload[c++] = bytes16[0];
	payload[c++] = bytes16[1];

	return payload;
}

uint8_t *BusModule::ConvertToBytesPayloadWithIndex(BusData data)
{
	uint8_t *payload;
	uint8_t bytes[sizeof(float)];
	payload = (uint8_t *)malloc(sizeof(uint8_t) * BUS_MODULE_PAYLOAD_SIZE_WITH_INDEX);
	payload[0] = DAY;
	payload[1] = (uint8_t)data.nmeaData.day;
	payload[2] = MONTH;
	payload[3] = (uint8_t)data.nmeaData.month;
	payload[4] = YEAR;
	payload[5] = (uint8_t)data.nmeaData.year;
	payload[6] = HOURS;
	payload[7] = (uint8_t)data.nmeaData.hours;
	payload[8] = MINUTES;
	payload[9] = (uint8_t)data.nmeaData.minutes;
	payload[10] = SECONDS;
	payload[11] = (uint8_t)data.nmeaData.seconds;
	payload[12] = STATUS;
	payload[13] = (uint8_t)data.nmeaData.status;

	payload[14] = LATITUDE;
	*(float *)(bytes) = data.nmeaData.latitude;
	payload[15] = bytes[0];
	payload[16] = bytes[1];
	payload[17] = bytes[2];
	payload[18] = bytes[3];

	payload[19] = LONGITUDE;
	*(float *)(bytes) = data.nmeaData.longitude;
	payload[20] = bytes[0];
	payload[21] = bytes[1];
	payload[22] = bytes[2];
	payload[23] = bytes[3];

	payload[24] = SPEED;
	*(float *)(bytes) = data.nmeaData.speed;
	payload[25] = bytes[0];
	payload[26] = bytes[1];
	payload[27] = bytes[2];
	payload[28] = bytes[3];

	payload[29] = TEMP;
	payload[30] = (uint8_t)data.temperature;
	payload[31] = HUMIDITY;
	payload[32] = (uint8_t)data.humidity;

	// payload[33] = NOISERAW;
	// *(float *)(bytes) = data.noiseData.meanNoiseRaw;
	// payload[34] = bytes[0];
	// payload[35] = bytes[1];
	// payload[36] = bytes[2];
	// payload[37] = bytes[3];

	return payload;
}

uint8_t *BusModule::ConvertToCompactBytesPayload(BusData data)
{
	return NULL;
}

void BusModule::SetupBlinkLedTimer(HardwareTimer *timer)
{
	uint32_t LED_RATE = 1 * 1000 * 1000; // in microseconds
	timer->pause();
	timer->setOverflow(LED_RATE, MICROSEC_FORMAT);
	timer->attachInterrupt(std::bind(BusModule::BlinkLedHandler,timer));
	pinMode(pinLedSucess, OUTPUT);
	pinMode(pinLedError, OUTPUT);
	// loggerSerial->println("CABOU, EH TETRA");
}

void BusModule::SetupDataTimer(HardwareTimer *timer, bool activateGps,
							   bool activateTemp, bool activateBlink)
{
	uint32_t second = 1 * 1000 * 1000; // in microseconds
	timer->pause();
	timer->setOverflow(second * routineTime, MICROSEC_FORMAT);
	// timer->setPrescaleFactor(32959);
	// timer->setOverflow(131091, TICK_FORMAT);
	timer->attachInterrupt(std::bind(BusModule::SaveDataHandler,timer));

	timer->setMode(1, TIMER_OUTPUT_COMPARE);
	timer->setCaptureCompare(1, second * 1, MICROSEC_COMPARE_FORMAT);
	timer->attachInterrupt(1, std::bind(BusModule::SendDataHandler,timer));

	// activateTemp = false;
	// activateGps = false;
	// activateBlink = false;

	if (activateTemp) // activateTemp
	{
		timer->setMode(2, TIMER_OUTPUT_COMPARE);
		timer->setCaptureCompare(2, second * 2, MICROSEC_COMPARE_FORMAT);
		timer->attachInterrupt(2, std::bind(BusModule::ReadTemperatureHandler,timer));
	}
	if (activateGps) // activateGps
	{
		timer->setMode(3, TIMER_OUTPUT_COMPARE);
		timer->setCaptureCompare(3, second * 1, MICROSEC_COMPARE_FORMAT);
		timer->attachInterrupt(3, std::bind(BusModule::ReadGpsHandler,timer));
	}
	if (activateBlink) // activateBlink
	{
		timer->setMode(4, TIMER_OUTPUT_COMPARE);
		timer->setCaptureCompare(4, second * 1, MICROSEC_COMPARE_FORMAT);
		timer->attachInterrupt(4, std::bind(BusModule::BlinkLedHandler,timer));
	}
	// loggerSerial->println("CABOU, EH TETRA DATA");
	// loggerSerial->println((String)timer->getPrescaleFactor());
	// loggerSerial->println((String)timer->getOverflow(TICK_FORMAT));
	timer->resume();
}

void BusModule::SetupAccelTimer(HardwareTimer *timer)
{
	timer->pause();
	// loggerSerial->println("ANTES DO SET ACCEL");
	timer->setMode(1, TIMER_OUTPUT_COMPARE);
	// timer->setMode(2, TIMER_OUTPUT_COMPARE);
	// timer->setMode(3, TIMER_OUTPUT_COMPARE);
	// timer->setMode(4, TIMER_OUTPUT_COMPARE);
	timer->setOverflow(2000, MICROSEC_FORMAT);
	// timer->setOverflow(accelSampling, HERTZ_FORMAT);
	timer->setCaptureCompare(1, 800, MICROSEC_COMPARE_FORMAT);
	// timer->setCaptureCompare(2, second * 2, MICROSEC_COMPARE_FORMAT);
	// timer->setCaptureCompare(3, second * 2, MICROSEC_COMPARE_FORMAT);
	// timer->setCaptureCompare(4, second * 2, MICROSEC_COMPARE_FORMAT);
	timer->attachInterrupt(std::bind(BusModule::ReadAccelHandler,timer));
	// timer->attachInterrupt(1, BusModule::ReadAccelHandler);
	timer->attachInterrupt(1, std::bind(BusModule::ReadNoiseHandler,timer));
	// timer->attachInterrupt(2, BusModule::ReadTemperatureHandler);
	// timer->attachInterrupt(3, BusModule::MicrophoneHandler);
	// timer->attachInterrupt(4, BusModule::BlinkLedHandler);
	// loggerSerial->println("CABOU, EH TETRA ACCEL");
	timer->resume();
}

void BusModule::SetupNoiseTimer(HardwareTimer *timer)
{
	timer->pause();
	// loggerSerial->println("ANTES DO SET NOISE");
	// timer->setMode(1, TIMER_OUTPUT_COMPARE);
	// timer->setMode(2, TIMER_OUTPUT_COMPARE);
	// timer->setMode(3, TIMER_OUTPUT_COMPARE);
	// timer->setMode(4, TIMER_OUTPUT_COMPARE);
	// timer->setOverflow(noiseSampling, HERTZ_FORMAT);
	// timer->setCaptureCompare(1, 100, MICROSEC_COMPARE_FORMAT);
	// timer->setCaptureCompare(2, second * 2, MICROSEC_COMPARE_FORMAT);
	// timer->setCaptureCompare(3, second * 2, MICROSEC_COMPARE_FORMAT);
	// timer->setCaptureCompare(4, second * 2, MICROSEC_COMPARE_FORMAT);
	// timer->attachInterrupt(BusModule::ReadNoiseHandler);
	// timer->attachInterrupt(1, BusModule::ReadNoiseHandler);
	// timer->attachInterrupt(2, BusModule::ReadTemperatureHandler);
	// timer->attachInterrupt(3, BusModule::MicrophoneHandler);
	// timer->attachInterrupt(4, BusModule::BlinkLedHandler);
	// loggerSerial->println("CABOU, EH TETRA NOISE");
	// timer->resume();
}

void BusModule::SaveDataHandler(HardwareTimer *timer)
{ // Should execute once per ROUTINE_TIME
	// Handler responsible for:

	// * saving current BusData to the output buffer vector
	// * cleaning the BusData
	// loggerSerial->println("SAVE DATA");
	// loggerSerial->println((String)timer->getCount(MICROSEC_FORMAT));

	uint32_t t1 = micros();
	// busData.nmeaData = gpsModule.GetNmeaRmc();
	// busData.temperature = temperatureModule.temperature;
	// busData.humidity = temperatureModule.humidity;
	// busData.meanNoiseRaw = noiseModule.GetMeanNoiseRaw(false);
	// busData.maxNoiseDb = noiseModule.GetMaxNoiseDb(true);
	// busData.accelerometerData = accelerometerModule.data;

	// Definir tamanho limite
	// TODO: incluir parâmetro na remoção dos pacotes para remover o último pacote
	// TODO: se a alocação de DataList falhar, então remover o último pacote e tentar realocar
	// TODO: limite do máximo de pacotes, se chegar no máximo então remover o último e tentar alocar um novo
	DataList *old = dataList;
	dataList = new DataList();
	if (dataList == NULL)
	{
		// Bem, se isso acontecer, deu merda
		loggerSerial->print("LASCOU, NÃO TEM MAIS MEMÓRIA");
		//
	}
	else
	{
		SetErrorValues(&dataList->data);
		dataList->nextNode = old;
		nPackets++;

		dataList->data.nmeaData = gpsModule.GetNmeaRmc();
		dataList->data.temperature = temperatureModule.temperature;
		dataList->data.humidity = temperatureModule.humidity;
		dataList->data.noiseData.maxNoiseDb = (float)noiseModule.data.maxNoiseDb;
		dataList->data.noiseData.noiseCount[0] = noiseModule.data.noiseCount[0];
		dataList->data.noiseData.noiseCount[1] = noiseModule.data.noiseCount[1];
		dataList->data.noiseData.noiseCount[2] = noiseModule.data.noiseCount[2];
		dataList->data.noiseData.noiseCount[3] = noiseModule.data.noiseCount[3];
		dataList->data.noiseData.noiseCount[4] = noiseModule.data.noiseCount[4];
		dataList->data.accelerometerData.jxCount0 = accelerometerModule.data.jxCount0;
		dataList->data.accelerometerData.jyCount0 = accelerometerModule.data.jyCount0;
		dataList->data.accelerometerData.jzCount0 = accelerometerModule.data.jzCount0;

		printBusSensorsDataCompact(&(dataList->data));
	}

	noiseModule.Clean();
	accelerometerModule.Clean();
	accelerometerModule.ReadAccelerometer();
	uint32_t t2 = micros();
	loggerSerial->print("TIME R SAVE:");
	loggerSerial->println((String)(t2 - t1));
}

void BusModule::SendDataHandler(HardwareTimer *timer)
{ // Should execute atleast 2 times per ROUTINE_TIME,
	// Handler responsible for:
	// * Checking for ACKNOWNLEDGMENTs in the communicationModule
	// * Removing the ACKNOWLEDGED packets from the output buffer vector
	// * Sending the latest BusData to the Master
	// loggerSerial->println("SEND DATA");
	// loggerSerial->println((String)timer->getCount(MICROSEC_FORMAT));
	uint32_t t1 = micros();
	int channel = 1;
	// Verify the current execution against the ROUTINE_TIME (overflow from timer)
	// Configuração da próxima tentativa
	msRoutineTime = timer->getOverflow(MICROSEC_FORMAT);
	uint32_t cur_compare = timer->getCaptureCompare(channel, MICROSEC_COMPARE_FORMAT);
	uint32_t difference = msRoutineTime - cur_compare;
	uint32_t next_compare = msRoutineTime / (sendRetries + 1);
	if (difference > next_compare)
		next_compare += cur_compare;
	timer->setCaptureCompare(channel, next_compare, MICROSEC_COMPARE_FORMAT);

	// * Checking for ACKNOWNLEDGMENTs
	// uint8_t *ackPayload = NULL;
	uint8_t ackPayload[MAX_BUFFER_SIZE];
	uint16_t size = 0;
	communicationModule.ReceiveData(2, ackPayload, &size);

	loggerSerial->print("SEND nPackets:");
	loggerSerial->print((String)nPackets);
	loggerSerial->print(" ");
	loggerSerial->print((String)communicationModule.receivedDataValid);
	loggerSerial->println(" ");
	// Validamos a mensagem recebida
	if (communicationModule.receivedDataValid && communicationModule.ValidateAck(ackPayload, size))
	{
		loggerSerial->println("ENTROU AQUI");
		RemovePacketFromList(ackPayload);
	}
	if (nPackets > 0)
	{
		// Sending latest BusData to the Master
		uint8_t *currentPayload = NULL;
		// currentPayload = ConvertToBytesPayload(busData);
		currentPayload = ConvertToBytesPayload(dataList->data);

		communicationModule.SendData(currentPayload);

		// if (communicationModule.communicationSuccess == true)
		// {
		// 	loggerSerial->println("COMMUNICATION SUCCESS");
		// 	// BlinkLed(pinLedSucess);
		// }
		// else
		// {
		// 	loggerSerial->println("COMMUNICATION ERROR");
		// 	// BlinkLed(pinLedError);
		// }

		// Talvez seja bom validar esses free? Não sei.
		// if (ackPayload != NULL)
		// 	free(ackPayload);
		if (currentPayload != NULL)
			free(currentPayload);
	}
	uint32_t t2 = micros();
	loggerSerial->print("TIME R SEND:");
	loggerSerial->println((String)(t2 - t1));
}

void BusModule::ReadTemperatureHandler(HardwareTimer *timer)
{
	// loggerSerial->println("R TEMP");
	// loggerSerial->println((String)timer->getCount(MICROSEC_FORMAT));
	uint32_t t1 = micros();
	int channel = 2;
	msRoutineTime = timer->getOverflow(MICROSEC_FORMAT);
	uint32_t cur_compare = timer->getCaptureCompare(channel, MICROSEC_COMPARE_FORMAT);
	uint32_t difference = msRoutineTime - cur_compare;
	uint32_t next_compare = msRoutineTime / (tempRetries + 1);
	temperatureModule.Read(false);
	busData.temperature = temperatureModule.temperature;
	busData.humidity = temperatureModule.humidity;
	if (temperatureModule.error)
	{
		// Se der erro, vamos programar mais uma leitura
		// dentro do ROUTINE_TIME disponível, somente se possível
		// Verify the current execution against the ROUTINE_TIME (overflow from timer)

		if (difference > next_compare)
			next_compare += cur_compare;
	}
	timer->setCaptureCompare(channel, next_compare, MICROSEC_COMPARE_FORMAT);

	uint32_t t2 = micros();
	loggerSerial->print("TIME R TEMP:");
	loggerSerial->println((String)(t2 - t1));
}

void BusModule::ReadNoiseHandler(HardwareTimer *timer)
{
	// loggerSerial->println("R NOISE");
	// loggerSerial->println((String)timer->getCount(MICROSEC_FORMAT));
	uint32_t t1 = micros();
	noiseModule.Read();
	uint32_t t2 = micros();
	// loggerSerial->println("*********");
	// loggerSerial->print("TIME R NOISE:");
	// loggerSerial->println((String)(t2 - t1));
	// busData.meanNoiseRaw = noiseModule.GetMeanNoiseRaw();
	// loggerSerial->println((String)busData.meanNoiseRaw);
	// loggerSerial->println((String)noiseModule.data.samplesNoiseRaw);

	// int channel = 1;
	// // Verify the current execution against the ROUTINE_TIME (overflow from timer)
	// uint32_t ROUTINE_TIME = timer->getOverflow(MICROSEC_FORMAT);
	// uint32_t N_MAX_EXECUTION = 20;
	// uint32_t cur_compare = timer->getCaptureCompare(channel, MICROSEC_COMPARE_FORMAT);
	// uint32_t difference = ROUTINE_TIME - cur_compare;
	// uint32_t next_compare = ROUTINE_TIME / (N_MAX_EXECUTION + 1);
	// if (difference > next_compare)
	// 	next_compare += cur_compare;
	// else
	// {
	// 	loggerSerial->print("samplesNoiseRaw: ");
	// 	loggerSerial->println((String)noiseModule.data.samplesNoiseRaw);
	// 	busData.meanNoiseRaw = noiseModule.GetMeanNoiseRaw();
	// 	loggerSerial->print("meanNoiseRaw: ");
	// 	loggerSerial->println((String)busData.meanNoiseRaw);
	// }
	// timer->setCaptureCompare(channel, next_compare, MICROSEC_COMPARE_FORMAT);
}

void BusModule::ReadGpsHandler(HardwareTimer *timer)
{
	// loggerSerial->println("R GPS");
	// loggerSerial->println((String)timer->getCount(MICROSEC_FORMAT));
	uint32_t t1 = micros();
	gpsModule.Read();
	busData.nmeaData = gpsModule.GetNmeaRmc();
	uint32_t t2 = micros();
	loggerSerial->print("TIME R GPS:");
	loggerSerial->println((String)(t2 - t1));
}

void BusModule::ReadAccelHandler(HardwareTimer *timer)
{ // Isso tem que executar em uma frequência de 500Hz, a cada 2ms.
	// loggerSerial->println("R ACCEL");
	// loggerSerial->println((String)timer->getCount(MICROSEC_FORMAT));
	uint32_t t1 = micros();
	// accelerometerModule.CalculateMeanJerk();
	accelerometerModule.RealTimeLongitudinalJerk();
	uint32_t t2 = micros();
	// loggerSerial->print("TIME R ACCEL:");
	// loggerSerial->println((String) (t2-t1));
	// loggerSerial->print("Jx: ");
	// loggerSerial->println(String(accelerometerModule.meanJerkValueX));
	// loggerSerial->print("Jy: ");
	// loggerSerial->println(String(accelerometerModule.meanJerkValueY));
	// loggerSerial->print("Jz: ");
	// loggerSerial->println(String(accelerometerModule.meanJerkValueZ));
}

void BusModule::BlinkLed(uint32_t pinLed)
{
	pinCurrentLed = pinLed;
	BusModule::TimerBlinkLed->resume();
}

void BusModule::BlinkLedHandler(HardwareTimer *timer)
{
	toggle ^= 1;
	if (pinCurrentLed != 0)
	{
		loggerSerial->println("PISCOU O LED");
		digitalWrite(pinCurrentLed, toggle);
		// digitalWrite(pinCurrentLed, HIGH);
		// delay(1000);
		// digitalWrite(pinCurrentLed, LOW);
	}
	if (toggle == 0)
		timer->pause();
}

void BusModule::printBusSensorsData(BusData *in)
{
	if (loggerSerial)
	{
		if (in == NULL)
			in = &busData;
		loggerSerial->println("<BusData>");
		loggerSerial->print("Date: ");
		loggerSerial->print(in->nmeaData.day);
		loggerSerial->print("/");
		loggerSerial->print(in->nmeaData.month);
		loggerSerial->print("/");
		loggerSerial->println(in->nmeaData.year);
		loggerSerial->print("Time: ");
		loggerSerial->print(in->nmeaData.hours);
		loggerSerial->print(":");
		loggerSerial->print(in->nmeaData.minutes);
		loggerSerial->print(":");
		loggerSerial->println(in->nmeaData.seconds);
		if (in->nmeaData.status == 0x41) //0x41 ASCII to A
		{
			loggerSerial->print("Location: ");
			loggerSerial->print("Lat: ");
			loggerSerial->println(in->nmeaData.latitude);
			loggerSerial->print("Lon: ");
			loggerSerial->println(in->nmeaData.longitude);
		}
		else
		{
			loggerSerial->println("Location: empty");
		}
		loggerSerial->print("Speed: ");
		loggerSerial->println(in->nmeaData.speed);
		loggerSerial->print("Temperature: ");
		loggerSerial->print(in->temperature);
		loggerSerial->println(" *C");
		loggerSerial->print("Humidity: ");
		loggerSerial->print(in->humidity);
		loggerSerial->println(" %");
		loggerSerial->print("maxNoiseDb: ");
		loggerSerial->println(in->noiseData.maxNoiseDb);
		// loggerSerial->print("Ax: ");
		// loggerSerial->print(in->accelerometerData.ax);
		// loggerSerial->print("\tAy: ");
		// loggerSerial->print(in->accelerometerData.ay);
		// loggerSerial->print("\tAz: ");
		// loggerSerial->println(in->accelerometerData.az);
		// loggerSerial->print("Gx: ");
		// loggerSerial->print(in->accelerometerData.gx);
		// loggerSerial->print("\tGy: ");
		// loggerSerial->print(in->accelerometerData.gy);
		// loggerSerial->print("\tGz: ");
		// loggerSerial->println(in->accelerometerData.gz);
		// loggerSerial->print("mJx: ");
		// loggerSerial->print(in->accelerometerData.meanJerkX);
		// loggerSerial->print("\tmJy: ");
		// loggerSerial->print(in->accelerometerData.meanJerkY);
		// loggerSerial->print("\tmJz: ");
		// loggerSerial->println(in->accelerometerData.meanJerkZ);
		// loggerSerial->print("Temperature (axcel): ");
		// loggerSerial->print(in->accelerometerData.T);
		// loggerSerial->println(" *C");
	}
}

void BusModule::printBusSensorsDataCompact(BusData *in)
{
	if (loggerSerial)
	{
		if (in == NULL)
			in = &busData;
		loggerSerial->println("<BusData>");
		loggerSerial->print("Date: ");
		loggerSerial->print(in->nmeaData.day);
		loggerSerial->print("/");
		loggerSerial->print(in->nmeaData.month);
		loggerSerial->print("/");
		loggerSerial->print(in->nmeaData.year);
		loggerSerial->print(" ");
		loggerSerial->print(in->nmeaData.hours);
		loggerSerial->print(":");
		loggerSerial->print(in->nmeaData.minutes);
		loggerSerial->print(":");
		loggerSerial->println(in->nmeaData.seconds);
		if (in->nmeaData.status == 0x41) //0x41 ASCII to A
		{
			loggerSerial->print("Location: ");
			loggerSerial->print(in->nmeaData.latitude);
			loggerSerial->print(", ");
			loggerSerial->println(in->nmeaData.longitude);
		}
		else
		{
			loggerSerial->println("Location: empty");
		}
		loggerSerial->print("Speed: ");
		loggerSerial->print(in->nmeaData.speed);
		loggerSerial->print(" Temp: ");
		loggerSerial->print(in->temperature);
		loggerSerial->print(" *C");
		loggerSerial->print(" Humi: ");
		loggerSerial->print(in->humidity);
		loggerSerial->print(" %");
		loggerSerial->println("");
		loggerSerial->print(" maxNoiseDb: ");
		loggerSerial->print((String)in->noiseData.maxNoiseDb);
		loggerSerial->print("noiseCount [0] ");
		loggerSerial->print((String)in->noiseData.noiseCount[0]);
		loggerSerial->print(" [1] ");
		loggerSerial->print((String)in->noiseData.noiseCount[1]);
		loggerSerial->print(" [2] ");
		loggerSerial->print((String)in->noiseData.noiseCount[2]);
		loggerSerial->print(" [3] ");
		loggerSerial->print((String)in->noiseData.noiseCount[3]);
		loggerSerial->print(" [4] ");
		loggerSerial->print((String)in->noiseData.noiseCount[4]);
		loggerSerial->println("");
		loggerSerial->print("jxCount0: ");
		loggerSerial->print(in->accelerometerData.jxCount0);
		loggerSerial->print("\tjyCount0: ");
		loggerSerial->print(in->accelerometerData.jyCount0);
		loggerSerial->print("\tjzCount0: ");
		loggerSerial->print(in->accelerometerData.jzCount0);
		// loggerSerial->println("");
		// loggerSerial->print("Ax: ");
		// loggerSerial->print(in->accelerometerData.ax);
		// loggerSerial->print("\tAy: ");
		// loggerSerial->print(in->accelerometerData.ay);
		// loggerSerial->print("\tAz: ");
		// loggerSerial->print(in->accelerometerData.az);
		// loggerSerial->print("\tGx: ");
		// loggerSerial->print(in->accelerometerData.gx);
		// loggerSerial->print("\tGy: ");
		// loggerSerial->print(in->accelerometerData.gy);
		// loggerSerial->print("\tGz: ");
		// loggerSerial->print(in->accelerometerData.gz);
		// loggerSerial->println("");
		// loggerSerial->print("mJx: ");
		// loggerSerial->print(in->accelerometerData.meanJerkX);
		// loggerSerial->print("\tmJy: ");
		// loggerSerial->print(in->accelerometerData.meanJerkY);
		// loggerSerial->print("\tmJz: ");
		// loggerSerial->print(in->accelerometerData.meanJerkZ);
		// loggerSerial->println("");
		// loggerSerial->print("Temperature (axcel): ");
		// loggerSerial->print(in->accelerometerData.T);
		// loggerSerial->print(" *C");
		loggerSerial->println("");
	}
}

void BusModule::RemovePacketFromList(uint8_t *data)
{
	// Só remove se mais de um pacote estiver pendente
	if (data != NULL && nPackets > 0)
	{
		DataList *current = dataList;
		DataList *previous = NULL;
		DataList *toRemove = NULL;
		// Deve-se verificar a data em todos os pacotes e remover o correspondente ao ACK
		while (current != NULL)
		{
			if (data[0] == (uint8_t)current->data.nmeaData.day &&
				data[1] == (uint8_t)current->data.nmeaData.month &&
				data[2] == (uint8_t)current->data.nmeaData.year &&
				data[3] == (uint8_t)current->data.nmeaData.hours &&
				data[4] == (uint8_t)current->data.nmeaData.minutes &&
				data[5] == (uint8_t)current->data.nmeaData.seconds)
			{
				toRemove = current;
				if (previous != NULL)
					previous->nextNode = current->nextNode;
				nPackets--;
				break;
			}
			previous = current;
			current = current->nextNode;
		}
		// Caso o nodo a ser removido seja o primeiro
		if (previous == NULL)
			dataList = current->nextNode;
		// Talvez seja bom validar o free? Não sei.
		if (toRemove != NULL)
			free(toRemove);
	}
}