#ifndef BUSSENSORS_H_
#define BUSSENSORS_H_

#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SimpleDHT.h>
#include <HardwareTimer.h>
#include "AccelerometerModule.h"
#include "CommunicationModule/CommunicationModule.h"
#include "GpsModule.h"
#include "NoiseModule.h"
#include "TemperatureModule.h"
#include "CommunicationModule/LoggerSerial.h"

// busModule Sensors and Modules Configuration
/* Baudrate for software UART used for NEO-7M communication */
#define NEO7M_BAUDRATE 9600
#define NEO7M_TX_PIN 2 // Pinos somente para o Arduino UNO
#define NEO7M_RX_PIN 3 // A HardwareSerial Serial1 é utilizada no Arduino Due

// LoRaMESH Configuration
#define LORAMESH_COMM_BAUDRATE 9600
#define LORAMESH_TX_COMM_PIN 4 // Pinos somente para o Arduino UNO, Serial2 é utilizada no Arduino Due
#define LORAMESH_RX_COMM_PIN 5 // Pinos da interface de comando

#define LORAMESH_TRANS_BAUDRATE 9600
#define LORAMESH_TX_TRANS_PIN 6 // Pinos somente para o Arduino UNO, Serial3 é utilizada no Arduino Due
#define LORAMESH_RX_TRANS_PIN 7 // Pinos da interface transparente

// #define DHT11_PIN 8				// Pino de dados do DHT11
#define DHT11_PIN PA15			// Pino de dados do DHT11
#define DHT11_SAMPLINGTIME 3000 // DHT11 sampling rate is 1HZ.
#define DHT_TYPE DHT11			// DHT11

// #define MIC_APIN A0 // Pino analógico do microfone KY-038
// #define MIC_APIN (uint8_t)'6' // Pino analógico do microfone KY-038
// #define MIC_DPIN 9			  // Pino digital do microfone KY-038
#define MIC_APIN PA1 // Pino analógico do microfone KY-038
#define MIC_DPIN PB9 // Pino digital do microfone KY-038

#define DAY 0x01	   // dados uint8_t
#define MONTH 0x02	 // dados uint8_t
#define YEAR 0x03	  // dados uint8_t
#define HOURS 0x04	 // dados uint8_t
#define MINUTES 0x05   // dados uint8_t
#define SECONDS 0x06   // dados uint8_t
#define STATUS 0x07	// dados uint8_t
#define LATITUDE 0x08  // dados float (4x uint8_t)
#define LONGITUDE 0x09 // dados float (4x uint8_t)
#define SPEED 0x0A	 // dados float (4x uint8_t)
#define TEMP 0x0B	  // dados uint8_t
#define HUMIDITY 0x0C  // dados uint8_t
// #define NOISERAW 0x0D  // dados float (4x uint8_t)
#define MAXNOISEDB 0x0D  // dados float (4x uint8_t)
#define NOISECOUNT0 0x0E // dados uint16_t (2x uint8_t)
#define NOISECOUNT1 0x0F // dados uint16_t (2x uint8_t)
#define NOISECOUNT2 0x10 // dados uint16_t (2x uint8_t)
#define NOISECOUNT3 0x11 // dados uint16_t (2x uint8_t)
#define NOISECOUNT4 0x12 // dados uint16_t (2x uint8_t)
// #define NOISEDB     0x0E    // dados uint8_t

#define JXCOUNT0 0x13 // dados uint16_t (2x uint8_t)
#define JYCOUNT0 0x14 // dados uint16_t (2x uint8_t)
#define JZCOUNT0 0x15 // dados uint16_t (2x uint8_t)

#define BUS_MODULE_N_DATA 21
#define BUS_MODULE_PAYLOAD_SIZE 41
#define BUS_MODULE_PAYLOAD_SIZE_WITH_INDEX BUS_MODULE_N_DATA + BUS_MODULE_PAYLOAD_SIZE

// Needs to be defined with the correct Device ID to
// be used with STM32F103C8 if the main serial is being used to debug
#define RADIOENGELORAMESH_DEFAULT_DEVICE_ID 3

typedef enum
{
	BUSMOD_ARDUINO_UNO,
	BUSMOD_ARDUINO_DUE,
	BUSMOD_STM32F103C8_128K,
} BusModuleType;

typedef struct
{
	NmeaRmc nmeaData;
	byte temperature;
	byte humidity;
	NoiseSendData noiseData;
	AxcelSendData accelerometerData;
} BusData;

typedef struct DataList
{
	BusData data;
	DataList *nextNode;
} DataList;

class BusModule
{
private:
	static LoggerSerial *loggerSerial;

	static BusModuleType moduleType;

	// Sensor de Temperatura e Humidade do Ar
	static TemperatureModule temperatureModule;

	// Sensor de Ruído - Microfone
	static NoiseModule noiseModule;

	// Módulo de Localização
	static GpsModule gpsModule;

	// Módulo de Comunicação
	static CommunicationModule communicationModule;

	// Módulo de Acelerômetro
	static AccelerometerModule accelerometerModule;

	static BusData busData;
	static DataList *dataList;
	static uint32_t nPackets;

	static uint32_t pinLedSucess;
	static uint32_t pinLedError;
	static uint32_t pinCurrentLed;
	static uint32_t toggle;

	static HardwareTimer *TimerBlinkLed;
	static HardwareTimer *TimerData;
	static HardwareTimer *TimerAccel;
	static HardwareTimer *TimerNoise;

	static uint32_t routineTime;   // in seconds
	static uint32_t msRoutineTime; // in MICROSEC_FORMAT
	static uint8_t sendRetries;
	static uint8_t tempRetries;
	static uint8_t gpsRetries;
	static uint8_t accelSampling; // Hz
	static uint8_t noiseSampling; // Hz

public:
	BusModule();
	static void Setup(BusModuleType moduleType, uint32_t pinLedSucess,
					  uint32_t pinLedError, TwoWire *accelWire,
					  SomeSerial *gpsSerial, SomeSerial *transparentSerial,
					  SomeSerial *commandSerial = NULL);
	static void SetupLogger(LoggerSerial *loggerSerial);
	static void SetupTimers(uint32_t routineTime, uint8_t sendRetries, uint8_t tempRetries,
							uint8_t gpsRetries, uint8_t accelSampling, uint8_t noiseSampling);
	static void SetupTimerGpsOnly(uint32_t routineTime, uint8_t sendRetries, uint8_t gpsRetries);
	static void SetupTimerAxcelOnly(uint8_t accelSampling);
	static void SetupTimerNoiseOnly(uint8_t accelSampling);
	static void ExecuteRoutine(void);
	static void SetErrorValues(void);
	static void SetErrorValues(BusData *busData);

	static uint8_t *ConvertToBytesPayload(BusData data);
	static uint8_t *ConvertToBytesPayloadWithIndex(BusData data);
	static uint8_t *ConvertToCompactBytesPayload(BusData data);

	static void SetupBlinkLedTimer(HardwareTimer *timer);
	static void BlinkLed(uint32_t pinLed);
	static void BlinkLedHandler(HardwareTimer *timer);

	static void SetupDataTimer(HardwareTimer *timer, bool activateGps = true,
							   bool activateTemp = true, bool activateBlink = true);
	static void SetupAccelTimer(HardwareTimer *timer);
	static void SetupNoiseTimer(HardwareTimer *timer);
	static void SaveDataHandler(HardwareTimer *timer);
	static void SendDataHandler(HardwareTimer *timer);
	static void ReadTemperatureHandler(HardwareTimer *timer);
	static void ReadNoiseHandler(HardwareTimer *timer);
	static void ReadGpsHandler(HardwareTimer *timer);
	static void ReadAccelHandler(HardwareTimer *timer);

	static void printBusSensorsData(BusData *in);
	static void printBusSensorsDataCompact(BusData *in);

	static void RemovePacketFromList(uint8_t *data);
	static char *GetCurrentJson(void);
};

#endif /* BUSSENSORS_H_ */
