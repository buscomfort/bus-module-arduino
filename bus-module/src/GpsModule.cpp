#include "GpsModule.h"

int fromHex(char a)
{
	if (a >= 'A' && a <= 'F')
		return a - 'A' + 10;
	else if (a >= 'a' && a <= 'f')
		return a - 'a' + 10;
	else
		return a - '0';
}

GpsModule::GpsModule()
{
	loggerSerial == NULL;
	hSerialGps = NULL;
	neo7mBaudRate = -1;
	numberRetries = 0;
	currentAttempt = 0;
	isChecksumTerm = false;
	parity = -1;
	validMessage = false;
}

void GpsModule::Setup(GpsModuleType moduleType, uint32_t timeout, uint8_t numberRetries, SomeSerial *gpsSerial, uint32_t baudRate)
{
	GpsModule::moduleType = moduleType;
	GpsModule::timeout = timeout;
	GpsModule::numberRetries = numberRetries;
	if (moduleType == GPSMOD_NEO7M)
	{
		hSerialGps = gpsSerial;
		/* filter not used baudrates */
		neo7mBaudRate = baudRate;
		hSerialGps->begin(baudRate);
#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
		if (hSerialGps->isSoftwareSerial() && hSerialGps->thisSoftwareSerial->isListening())
			hSerialGps->thisSoftwareSerial->stopListening();
#endif
	}
}

void GpsModule::SetupLogger(LoggerSerial *loggerSerial)
{
	if (loggerSerial == NULL)
	{
		GpsModule::loggerSerial = new LoggerSerial();
	}
	else
		GpsModule::loggerSerial = loggerSerial;
}

void GpsModule::Read(void)
{
	char *rmcMessage = ScanNmea("$GPRMC");
	if (rmcMessage == NULL || !validMessage)
	{
		nmeaRmc.day = -1;
		nmeaRmc.month = -1;
		nmeaRmc.year = -1;
		nmeaRmc.hours = -1;
		nmeaRmc.minutes = -1;
		nmeaRmc.seconds = -1;
		nmeaRmc.status = -1;
		nmeaRmc.latitude = -1;
		nmeaRmc.longitude = -1;
		nmeaRmc.speed = -1;
	}
	else
	{
		nmeaRmc = ConvertToRmc(rmcMessage);
	}
}

NmeaRmc GpsModule::GetNmeaRmc(void)
{
	return nmeaRmc;
}

SomeSerial *GpsModule::InitSerial(uint8_t rxPin, uint8_t txPin, uint32_t baudRate)
{
#ifdef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	static SomeSerial gpsSerialCommands(&Serial1);
#else
	static SomeSerial gpsSerialCommands(rxPin, txPin);
#endif
	/* filter not used baudrates */
	gpsSerialCommands.begin(baudRate);
	GpsModule::neo7mBaudRate = baudRate;
	hSerialGps = &gpsSerialCommands;
#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialGps->isSoftwareSerial() && hSerialGps->thisSoftwareSerial->isListening())
		hSerialGps->thisSoftwareSerial->stopListening();
#endif
	return &gpsSerialCommands;
}

void GpsModule::StartSerial(void)
{
	if (moduleType == GPSMOD_NEO7M)
	{
		hSerialGps->begin(neo7mBaudRate);
	}
}

void GpsModule::StopSerial(void)
{
	if (moduleType == GPSMOD_NEO7M)
	{
		hSerialGps->end();
	}
}

char GpsModule::GetCurrentChar(void)
{
#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialGps->isSoftwareSerial() && !hSerialGps->thisSoftwareSerial->isListening())
		hSerialGps->thisSoftwareSerial->listen();
#endif
	uint32_t localTimeout = timeout;
	// Fica bloqueado esperando ter dado disponível na porta GPS
	// Talvez seja bom colocar um contador limite depois
	while (!hSerialGps->available() && localTimeout != 0)
	{
		localTimeout--;
		delay(1);
	}
	if (localTimeout == 0)
	{
		currentAttempt++;
		currentChar = '\n';
	}
	else
		currentChar = hSerialGps->read();
	return currentChar;
}

void GpsModule::AppendSpelling(char a)
{
	currentSpelling[cSpelling++] = a;
	currentSpelling[cSpelling] = '\0';
}

void GpsModule::FlushSpelling(void)
{
	cSpelling = 0;
	currentSpelling[cSpelling] = '\0';
}

char *GpsModule::ScanNmea(char *messageCode) //$GPRMC, $GPGGA, $GPGSA, $GPGLL
{
	currentAttempt = 0;
	validMessage = false;
	StartSerial();
	FlushBuffer();
	FlushSpelling();
	while (currentAttempt < numberRetries)
	{
		GetCurrentChar();
		if (currentAttempt >= numberRetries)
		{
			return NULL;
		}
		if (currentChar == '$')
		{
			parity = 0;
			isChecksumTerm = false;
			validMessage = false;
			AppendSpelling(currentChar);
			for (int x = 0; x < 5; x++)
			{
				GetCurrentChar();
				if (currentAttempt >= numberRetries)
				{
					return NULL;
				}
				AppendSpelling(currentChar);
				parity ^= currentChar;
			}

			if (!strcmp(currentSpelling, messageCode))
			{
				do
				{
					GetCurrentChar();
					if (currentAttempt >= numberRetries)
					{
						return NULL;
					}
					AppendSpelling(currentChar);
					if (currentChar == '*')
					{
						isChecksumTerm = true;
						char msbChecksum = GetCurrentChar();
						if (currentAttempt >= numberRetries)
						{
							return NULL;
						}
						AppendSpelling(currentChar);
						char lsbChecksum = GetCurrentChar();
						if (currentAttempt >= numberRetries)
						{
							return NULL;
						}
						AppendSpelling(currentChar);
						uint8_t checksum = 16 * fromHex(msbChecksum) + fromHex(lsbChecksum);
						if (checksum == parity)
						{
							validMessage = true;
							// loggerSerial->println("VALID MESSAGE");
						}
						currentAttempt++;
						// loggerSerial->print("CHECKSUM: ");
						// loggerSerial->println(checksum);
						// loggerSerial->print("PARITY: ");
						// loggerSerial->println(parity);
						break;
					}
					if (!isChecksumTerm)
						parity ^= (uint8_t)currentChar;
				} while (currentChar != '\n');
				strcpy(message, currentSpelling);
				FlushSpelling();

				// Tem que desativar a escuta pq o GPS joga dados muito rápido e atrapalha as outras SoftwareSerial rodando
				StopSerial();
				return message;
			}
			else
			{
				FlushSpelling();
			}
		}
	}
	StopSerial();
	return NULL;
}

void GpsModule::loopToMainSerial(void)
{
	while (1)
	{
		while (hSerialGps->available() > 0)
		{
			byte gpsData = hSerialGps->read();
			Serial.write(gpsData);
		}
	}
}

void GpsModule::printNmeaRmc(NmeaRmc in)
{
	printf("Date: %d/%d/%d\nTime: %d:%d:%d\nStatus: %c\nLatitude: %f\nLongitude %f\nSpeed: %f\n",
		   in.day, in.month, in.year,
		   in.hours, in.minutes, in.seconds,
		   in.status, in.latitude, in.longitude, in.speed);
}

char *GpsModule::ConvertNmeaRmcToJson(NmeaRmc in)
{
	char *target;
	target = (char *)malloc(sizeof(char) * 50);
	sprintf(target, "{A:%d,B:%d,C:%d,D:%d,E:%d,F:%d,G:%c,H:%.5f,I:%.5f,J:%.4f}",
			in.day, in.month, in.year,
			in.hours, in.minutes, in.seconds,
			in.status, in.latitude, in.longitude, in.speed);
	return target;
}

NmeaRmc GpsModule::ConvertToRmc(char *rmcMessage)
{
	NmeaRmc target;
	char time[11], status, latitudeV[12], latitudeS, longitudeV[12], longitudeS, date[7];
	float speed;
	int c;
	// More general pattern:
	char *token, *str;
	char *tofree = NULL;

	tofree = str = strdup(rmcMessage); // We own str's memory now.
	for (int c = 0; ((token = strsep(&str, ","))); c++)
	{
		switch (c)
		{
		case 0: // identificador "$GPRMC"
			continue;
			break;
		case 1: // horário no formato "hhmmss.00" UTC
			strcpy(time, token);
			break;
		case 2: // estado
			sscanf(token, "%c", &status);
			break;
		case 3: // latitude em graus e minutos
			strcpy(latitudeV, token);
			break;
		case 4: // hemisfério norte (N) ou sul (S)
			sscanf(token, "%c", &latitudeS);
			break;
		case 5: // longitude em graus e minutos
			strcpy(longitudeV, token);
			break;
		case 6: // hemisfério leste (E) ou oeste (W)
			sscanf(token, "%c", &longitudeS);
			break;
		case 7: // velocidade no chão em nós
			// sscanf(token, "%f", &speed); // Aparentemente sscanf e sprinf não fazem conversão de string no Arduino
			speed = atof(token);
			break;
		case 8: // ângulo de rastreamento em graus verdadeiros
			break;
		case 9: // data no formato  "ddmmaa"
			strcpy(date, token);
			break;
		default: // ignora o restante (variação magnética e o checksum)
			break;
		}
	}
	if (tofree != NULL)
		free(tofree);

	target.speed = speed * 1.852; // Converter de nós para km/h (knots to km/h)

	// 48 é igual ao caractere '0'
	target.hours = (time[0] - 48) * 10 + (time[1] - 48);
	target.minutes = (time[2] - 48) * 10 + (time[3] - 48);
	target.seconds = (time[4] - 48) * 10 + (time[5] - 48);

	target.day = (date[0] - 48) * 10 + (date[1] - 48);
	target.month = (date[2] - 48) * 10 + (date[3] - 48);
	target.year = (date[4] - 48) * 10 + (date[5] - 48);

	target.status = status;

	target.latitude = (latitudeV[0] - 48) * 10 + (latitudeV[1] - 48) + (atof(latitudeV + 2) / 60);
	if (latitudeS == 'S')
		target.latitude *= -1;

	target.longitude = (longitudeV[0] - 48) * 100 + (longitudeV[1] - 48) * 10 + (longitudeV[2] - 48) + (atof(longitudeV + 3) / 60);
	if (longitudeS == 'W')
		target.longitude *= -1;

	return target;
}
NmeaRmc GpsModule::ScanNmeaRmc(char *messageCode)
{
	char *rmcMessage = ScanNmea(messageCode);
	NmeaRmc converted = ConvertToRmc(rmcMessage);
	// free(rmcMessage);
	return converted;
}

void GpsModule::FlushBuffer(void)
{
	while (hSerialGps->available() > 0)
	{
		hSerialGps->read();
	}
}

char *GpsModule::GetCurrentJson(void)
{
	char *jsonString;
	jsonString = ConvertNmeaRmcToJson(ConvertToRmc(ScanNmea("$GPRMC")));
	return jsonString;
}
