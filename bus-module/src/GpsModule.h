#ifndef GPSMODULE_H_
#define GPSMODULE_H_

#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SomeSerial.h>
#include <HardwareSerial.h>
#include "CommunicationModule/LoggerSerial.h"

#define BUFFER_SIZE 100

typedef enum
{
	GPSMOD_NEO7M,
} GpsModuleType;

typedef struct
{
	int hours, minutes, seconds;
	int day, month, year;
	char status;
	float latitude, longitude, speed;
} NmeaRmc;

class GpsModule
{
private:
	LoggerSerial *loggerSerial;

	GpsModuleType moduleType;

	SomeSerial *hSerialGps;
	uint32_t neo7mBaudRate;
	uint8_t numberRetries;
	uint32_t timeout;
	uint8_t currentAttempt;
	bool isChecksumTerm;
	uint8_t parity;
	bool validMessage;
	NmeaRmc nmeaRmc;

	char currentSpelling[BUFFER_SIZE]; // pilha de análise léxica
	char message[BUFFER_SIZE];
	int cSpelling;	// topo da pilha
	char currentChar; // caractere Atual
public:
	GpsModule();
	void Setup(GpsModuleType moduleType, uint32_t timeout, uint8_t numberRetries, SomeSerial *gpsSerial, uint32_t baudRate);
	void SetupLogger(LoggerSerial *loggerSerial);
	void Read(void);
	NmeaRmc GetNmeaRmc(void);

	SomeSerial *InitSerial(uint8_t rxPin, uint8_t txPin, uint32_t baudRate);

	void StartSerial(void);
	void StopSerial(void);
	char GetCurrentChar(void);
	void AppendSpelling(char a);
	void FlushSpelling(void);
	char *ScanNmea(char *messageCode);
	NmeaRmc ConvertToRmc(char *rmcMessage);
	NmeaRmc ScanNmeaRmc(char *messageCode);
	void loopToMainSerial(void);

	void printNmeaRmc(NmeaRmc in);
	char *ConvertNmeaRmcToJson(NmeaRmc in);

	void FlushBuffer(void);

	char *GetCurrentJson(void);
};

#endif /* GPSMODULE_H_ */
