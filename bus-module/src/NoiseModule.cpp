#include "NoiseModule.h"

NoiseModule::NoiseModule()
{
	loggerSerial == NULL;
	Clean();
	sensorType = NOISESENSOR_NONE;
	referenceVoltage = 3.3;
}

void NoiseModule::Setup(NoiseSensorType sensorType, uint8_t analogPin, int digitalPin)
{
	NoiseModule::sensorType = sensorType;
	if (sensorType == NOISESENSOR_KY038)
	{
		pinMode(analogPin, INPUT);
		// pintMode(digitalPin, INPUT);
		ky038AnalogPin = analogPin;
		ky038DigitalPin = digitalPin;
		referenceVoltage = 3.3;
	}
}

void NoiseModule::SetupLogger(LoggerSerial *loggerSerial)
{
	if (loggerSerial == NULL)
	{
		NoiseModule::loggerSerial = new LoggerSerial();
	}
	else
		NoiseModule::loggerSerial = loggerSerial;
}

void NoiseModule::Read(void)
{
	ReadNoiseRaw();

	// Baseado no nível de dB (Leq(A) do Barone2018)
	if (noiseDb < 65.0) // NOISE_COUNT_0 // (Leq < 65)
		data.noiseCount[0]++;
	else if (noiseDb < 70.0) // NOISE_COUNT_1 // (65 <= Leq < 70)
		data.noiseCount[1]++;
	else if (noiseDb < 75.0) // NOISE_COUNT_2 // (70 <= Leq < 75)
		data.noiseCount[2]++;
	else if (noiseDb < 80.0) // NOISE_COUNT_3 // (75 <= Leq < 80)
		data.noiseCount[3]++;
	else // NOISE_COUNT_4 // (Leq >= 80)
		data.noiseCount[4]++;
}

double NoiseModule::GetNoiseDb(double n)
{
	// 20 * log10(V_noise / V_ref) + dB_ref
	// return (20.0 * log10(GetNoiseVoltage(n) / referenceVoltage) + 1.0);

	//
	// return (20.0 * log10(GetNoiseVoltage(n) / 1.69459531027075) + 48.7);
	double x;

	// Equação ajustada a partir da segunda calibração manual
	// Utilizando resolução de 16 bits
	// x = exp((GetNoiseVoltage(n) - 1.31246713317529) / 0.095724611919799);

	// 1 equação ajustada a partir da terceira calibração manual
	// Utilizando resolução de 16 bits
	x = exp((GetNoiseVoltage(n) - 1.09222911525831) / 0.148736485507653);

	// 5 equação ajustada a partir da terceira calibração manual
	// Utilizando resolução de 16 bits
	// x = exp((GetNoiseVoltage(n) - 1.54994392639251) / 0.037226796850211);

	// return (exp((GetNoiseVoltage(n)-1.7267)/0.3549));

	// x = GetNoiseVoltage(n);

	return x;
}

double NoiseModule::GetNoiseVoltage(double n)
{
	return ((n * referenceVoltage) / 65535.0);
	// return (noiseRaw * (referenceVoltage / 1023.0));
}

double NoiseModule::ReadNoiseRaw(void)
{
	if (sensorType == NOISESENSOR_KY038)
	{
		analogReadResolution(16);
		noiseRaw = analogRead(ky038AnalogPin);
		noiseDb = GetNoiseDb(noiseRaw);
		// double sampleVoltage = noiseRaw * (referenceVoltage / 1023.0);
		// noiseVoltage += sampleVoltage * sampleVoltage;
		// noiseRawSum += noiseRaw * noiseRaw;

		// data.meanNoiseRaw = (data.meanNoiseRaw * data.samplesNoiseRaw + noiseRaw) / (data.samplesNoiseRaw + 1);
		data.samplesNoiseRaw++;

		if (noiseRaw > data.maxNoiseRaw)
		{
			data.maxNoiseRaw = noiseRaw;
			data.maxNoiseDb = noiseDb;
		}
		// if (data.samplesNoiseRaw == 100)
		// {
		// 	loggerSerial->print(GetNoiseDb(data.maxNoiseRaw));
		// 	loggerSerial->println(" ");
		// 	Clean();
		// }
		return noiseRaw;
	}
}

void NoiseModule::Clean(void)
{
	data.meanNoiseRaw = 0;
	data.samplesNoiseRaw = 0;
	data.maxNoiseRaw = 0;
	data.maxNoiseDb = 0;
	data.noiseCount[0] = 0;
	data.noiseCount[1] = 0;
	data.noiseCount[2] = 0;
	data.noiseCount[3] = 0;
	data.noiseCount[4] = 0;
	noiseRaw = 0;
	noiseDb = 0;
	noiseVoltage = 0;
	noiseRawSum = 0;
	return;
}

double NoiseModule::GetMeanNoiseRaw(bool clean) //chama cleanMeanNoiseRaw e limpa a variável
{
	double target = data.meanNoiseRaw;
	if (clean == true)
		Clean();
	return target;
}

double NoiseModule::GetMaxNoiseDb(bool clean)
{
	double target = GetNoiseDb(data.maxNoiseRaw);
	loggerSerial->println("==============");
	loggerSerial->println((String)target);
	if (clean == true)
		Clean();
	return target;
}
