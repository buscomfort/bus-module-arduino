
#ifndef NOISEMODULE_H_
#define NOISEMODULE_H_

#include <Arduino.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "CommunicationModule/LoggerSerial.h"

typedef enum
{
	NOISESENSOR_NONE,
	NOISESENSOR_KY038,
} NoiseSensorType;

typedef struct
{
	double maxNoiseDb;
	// Baseado no nível de dB (Leq(A) do Barone2018)
	uint16_t noiseCount[5];
	// NOISE_COUNT_0 // (Leq < 65)
	// NOISE_COUNT_1 // (65 <= Leq < 70)
	// NOISE_COUNT_2 // (70 <= Leq < 75)
	// NOISE_COUNT_3 // (75 <= Leq < 80)
	// NOISE_COUNT_4 // (Leq >= 80)
} NoiseSendData;

typedef struct
{
	double meanNoiseRaw; // 0x0D
	uint32_t samplesNoiseRaw;
	double maxNoiseRaw;
	double maxNoiseDb;
	// Baseado no nível de dB (Leq(A) do Barone2018)
	uint16_t noiseCount[5];
	// NOISE_COUNT_0 // (Leq < 65)
	// NOISE_COUNT_1 // (65 <= Leq < 70)
	// NOISE_COUNT_2 // (70 <= Leq < 75)
	// NOISE_COUNT_3 // (75 <= Leq < 80)
	// NOISE_COUNT_4 // (Leq >= 80)
} NoiseData;

class NoiseModule
{
private:
	LoggerSerial *loggerSerial;
	NoiseSensorType sensorType;

public:
	double noiseRaw;
	double noiseDb;
	double noiseRawSum;
	double noiseVoltage;
	double referenceVoltage;
	NoiseData data;

	// Sensor de Ruído - Microfone
	uint8_t ky038AnalogPin;
	int ky038DigitalPin;

	NoiseModule();
	void Setup(NoiseSensorType sensorType, uint8_t analogPin, int digitalPin);
	void SetupLogger(LoggerSerial *loggerSerial);
	void Read(void);

	double GetNoiseDb(double n);
	double GetNoiseVoltage(double n);
	double ReadNoiseRaw(void);
	void Clean(void);
	double GetMeanNoiseRaw(bool clean = false); //chama cleanMeanNoiseRaw e limpa a variável
	double GetMaxNoiseDb(bool clean = false);
};

#endif /* NOISEMODULE_H_ */