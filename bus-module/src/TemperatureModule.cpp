#include "TemperatureModule.h"

TemperatureModule::TemperatureModule()
{
	loggerSerial == NULL;
	SetErrorValues();
	numberRetries = 0;
	sensorType = TEMPSENSOR_NONE;
	dht11 = NULL;
	error = true;
}

void TemperatureModule::Setup(TemperatureSensorType sensorType, int dataPin, int samplingTime, int numberRetries)
{
	TemperatureModule::sensorType = sensorType;
	TemperatureModule::numberRetries = numberRetries;
	if (sensorType == TEMPSENSOR_DHT11)
	{
		dht11Pin = dataPin;
		dht11SamplingTime = samplingTime;
		// for DHT11,
		//      VCC: 5V or 3V
		//      GND: GND
		//      DATA: dataPin
		if (dht11 != NULL)
			delete dht11;
		dht11 = new SimpleDHT11(dataPin);
		dhtError = SimpleDHTErrSuccess;
	}
	error = false;
}

void TemperatureModule::SetupLogger(LoggerSerial *loggerSerial)
{
	if (loggerSerial == NULL)
	{
		TemperatureModule::loggerSerial = new LoggerSerial();
	}
	else
		TemperatureModule::loggerSerial = loggerSerial;
}

void TemperatureModule::Read(bool retry)
{
	if (sensorType == TEMPSENSOR_DHT11)
	{
		dhtError = SimpleDHTErrSuccess;

		if (retry)
		{
			for (int c = 0; c < numberRetries; c++)
			{
				ReadDht11();
				delay(dht11SamplingTime);
			}
		}
		else
			ReadDht11();
	}
}

void TemperatureModule::ReadDht11(void)
{
	if (dht11 != NULL)
	{
		if ((dhtError = dht11->read(&temperature, &humidity, NULL)) == SimpleDHTErrSuccess)
		{
			error = false;
			return;
		}
		SetErrorValues();
	}
	else
	{
		SetErrorValues();
	}
}

void TemperatureModule::SetErrorValues()
{
	error = true;
	temperature = 0xFF;
	humidity = 0xFF;
}

void TemperatureModule::printDHT11(void)
{
	loggerSerial->print((int)temperature);
	loggerSerial->print(" *C, ");
	loggerSerial->print((int)humidity);
	loggerSerial->println(" H");
}