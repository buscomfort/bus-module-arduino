#ifndef TEMPERATUREMODULE_H_
#define TEMPERATUREMODULE_H_

#include <Arduino.h>
#include <stdlib.h>
#include <stdbool.h>
#include "CommunicationModule/LoggerSerial.h"
#include <SimpleDHT.h>

typedef enum
{
	TEMPSENSOR_NONE,
	TEMPSENSOR_DHT11,
	TEMPSENSOR_DHT22
} TemperatureSensorType;

class TemperatureModule
{
private:
	LoggerSerial *loggerSerial;

	TemperatureSensorType sensorType;
public:
	byte temperature;
	byte humidity;
	bool error;
	int numberRetries;


	// Sensor de Temperatura e Humidade do Ar
	SimpleDHT11 *dht11;
	int dht11Pin;
	int dht11SamplingTime;
	int dhtError;

	TemperatureModule();
	void Setup(TemperatureSensorType sensorType,
			   int dataPin,
			   int samplingTime,
			   int numberRetries = 4);
	void SetupLogger(LoggerSerial *loggerSerial);
	void Read(bool retry = false);
	void ReadDht11(void);
	
	void SetErrorValues();
	void printDHT11(void);
};

#endif /* TEMPERATUREMODULE_H_ */