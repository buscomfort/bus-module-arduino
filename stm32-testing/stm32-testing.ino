
#include <HardwareSerial.h>
// HardwareSerial Serial3(USART3);
// or HardWareSerial Serial3(PB11, PB10);
//HardwareSerial Serial2(PB11, PB10);

#define LED_BUILTIN PC13
void setup()
{
    Serial2.begin( 9600);
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
    digitalWrite(LED_BUILTIN, HIGH);
    Serial2.println("TESTING");
    delay(2000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
}
